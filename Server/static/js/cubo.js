/*******************************/
/*          CONNECTION         */
/*******************************/
cubo_connected = false;

var HttpClient = function() {
    this.get = function(aUrl, aCallback) {
        var anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function() {
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
                aCallback(anHttpRequest.responseText);
        }
        anHttpRequest.open( "GET", aUrl, true );
        anHttpRequest.send( null );
    }
};

client = new HttpClient();

/*******************************/
/*          ANIMATION          */
/*******************************/
var cubex = -22,    // initial rotation
cubey = -38,
cubez = 0;

function rotate(variableName, degrees) {
    window[variableName] = window[variableName] + degrees;
    rotCube(cubex, cubey, cubez);
}

function rotCube(degx, degy, degz){
    segs = "rotateX("+degx+"deg) rotateY("+degy+"deg) rotateZ("+degz+"deg) translateX(0) translateY(0) translateZ(0)";
    document.getElementById('D3Cube').style.transform = segs;
}

function turnRight() {
    rotate("cubey", 90);
}

function turnLeft() {
    rotate("cubey", -90);
}

function flipCube() {
    rotate("cubez", -180);
}

function toggle_light(side){
    handle = document.getElementById(side);
    color = document.getElementById("color_picker").value;
    console.log("Clicked "+side+" to turn to "+color.substring(1,7));

    client.get("/"+side+"/"+color.substring(1,7), function(response){
        handle.style.backgroundColor=color;
    });

}