/********************************************
   Author: Carlos Neves
   Project: IDMind's Cubo
   Goals:
   1. Set up a WiFi Access Point
   2. Set up a HTTP Server
   3. Control the lights using the webpage
*/

#include <Adafruit_MMA8451.h>
#include <Adafruit_Sensor.h>
#include <PN532_HSU.h>
#include <PN532.h>
#include <NfcAdapter.h>
#include <Adafruit_NeoPixel.h>


/* NEOPixel Variables */
// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN_NeoPixel            6
#define PIN_PushButton          5
#define PIN_Vibrator            2

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      8

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN_NeoPixel, NEO_GRB + NEO_KHZ800);
int delayval = 500; // delay for half a second
int buttonState;             // the current reading from the input pin
int neoPixel_State = 0;
int reading = 0;
int blink_counter = 0;
unsigned long timeout = 10000;

int neopixel_blink[8][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
int neopixel_acc[8][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
int neopixel_nfc[8][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
int neopixel_http[8][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}};

int side2idx[6][4] = {
  {4, 5, 6, 7}, // Hand
  {0, 1, 4, 7}, // Star
  {0, 3, 4, 5}, // Heart  
  {2, 3, 5, 6}, // Lightning
  {1, 2, 6, 7}, // Bulb
  {0, 1, 2, 3}, // Empty
};

/* ESP Global Variables */
bool connections[4] = {false, false, false, false};
String esp_buffer = "";
String request[4] ={"", "", "", ""};
int MAX_ESP_SIZE = 1750;

/* Accelerometer MMA8451 */
Adafruit_MMA8451 mma = Adafruit_MMA8451();
float ACC_MAX = 8191;
float v_acc[3] = {0.0, 0.0, 0.0};
float led_angles[8][3] = {
  {0.785, -0.785, 0.785},
  {-0.785, -0.785, 0.785},
  {-0.785, 0.785, 0.785},
  {0.785, 0.785, 0.785},
  {0.785, -0.785, -0.785},
  {0.785, 0.785, -0.785},
  {-0.785, 0.785, -0.785},
  {-0.785, -0.785, -0.785},
};

/* Mode variable
 *  0 - All lights in rainbow
 *  1 - Inclination
 *  2 - NFC
 *  3 - WebServer
 */
int mode = 0;

void setup()
{
  delay(5000);
  // Start NEOPixels
  pixels.begin();               // Initialize NeoPixel Library
  pinMode(PIN_PushButton, INPUT);
  pinMode(PIN_Vibrator, OUTPUT);
  neopixel_start();

  // Communication to PC
  Serial.begin(115200);

  // Communication to ESP8266
  Serial1.begin(115200);
  //  Serial1.setTimeout(10000);

  while (!EspStart());
  delay(100);
  while (Serial1.available()) Serial1.read();
  
  // Accelerometer
  while(!mma.begin()) {
    Serial.println("Couldnt start");
    delay(100);
  }
  mma.setRange(MMA8451_RANGE_2_G);  
  neopixel_green_light(4);
  Serial.println("MMA8451 found!");    

  delay(1000);
}

void loop() {
  
  reading = digitalRead(PIN_PushButton);
  if(reading == HIGH){
    while(digitalRead(PIN_PushButton)==HIGH);
    if(mode < 3) mode++;
    else mode = 0;
    digitalWrite(PIN_Vibrator, HIGH);
    delay(100);
    digitalWrite(PIN_Vibrator, LOW);
    Serial.println("Changing to mode "+String(mode));
  }
  
  // Blinking all lights
  if(mode==0){
    blink_counter += 1;
    if (blink_counter == 500){
      for(int led=0; led<8; led++){
        neopixel_assign(led, random(0,256), random(0,256), random(0,256));  
      }
      blink_counter = 0;
    }
  }
  // NFC
  else if(mode==1){
    
  }
  // Inclination
  else if(mode==2){
    mma.read();
    Serial.print("X:\t"); Serial.println(mma.x); 
    Serial.print("Y:\t"); Serial.println(mma.y); 
    Serial.print("Z:\t"); Serial.println(mma.z);
    v_acc[0] = 2*float(mma.x)/ACC_MAX;
    v_acc[1] = 2*float(mma.y)/ACC_MAX; 
    v_acc[2] = 2*float(mma.z)/ACC_MAX;
    
    for(int led=0; led<8; led++){
      int led_val = int(min(255, max(0, 255*(led_angles[led][0]*v_acc[0]+led_angles[led][1]*v_acc[1]+led_angles[led][2]*v_acc[2]))));
      if(abs(mma.x) > 6000 or abs(mma.y) > 6000 or abs(mma.z) > 6000)
        neopixel_assign(led, led_val, 0, 0);
      else
        neopixel_assign(led, 0, led_val, 0);
    }
  }
  else if(mode==3){
    if (Serial1.available() > 0) HttpServer();
  }

  // Hardware update
  neopixel_update();
  
}

float int_product(float* v1, float* v2){
//  Serial.println("V1:\t"+String(v1[1])+"\t"+String(v1[2])+"\t"+String(v1[3]));
  Serial.println("V2:\t"+String(v2[1])+"\t"+String(v2[2])+"\t"+String(v2[3]));
  Serial.println("V3:\t"+String(v1[1]*v2[1])+"\t"+String(v1[2]*v2[2])+"\t"+String(v1[3]*v2[3]));
  return v1[1]*v2[1]+v1[2]*v2[2]+v1[3]*v2[3];
}
