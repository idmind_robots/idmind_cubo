void neopixel_start(){
  for(int i=0; i<NUMPIXELS;i++)
    pixels.setPixelColor(i, pixels.Color(0,0,255));
  pixels.show();
  delay(1000);
}

void neopixel_set_alarm(){
  for(int i=0; i<NUMPIXELS;i++)
    pixels.setPixelColor(i, pixels.Color(255,0,0));
  pixels.show();
//  delay(1000);
}

void neopixel_green_light(int idx){
  pixels.setPixelColor(idx, pixels.Color(0,255,0));
  pixels.show();
//  delay(10);
}

void neopixel_red_light(int idx){
  pixels.setPixelColor(idx, pixels.Color(255,0,0));
  pixels.show();
//  delay(10);
}

void neopixel_blue_light(int idx){
  pixels.setPixelColor(idx, pixels.Color(0,0,255));
  pixels.show();
//  delay(10);
}

void neopixel_assign(int idx, int red, int green, int blue){
  if(mode==0){
    neopixel_blink[idx][0] = red;
    neopixel_blink[idx][1] = green;
    neopixel_blink[idx][2] = blue;
  }
  else if(mode==1){
    neopixel_acc[idx][0] = red;
    neopixel_acc[idx][1] = green;
    neopixel_acc[idx][2] = blue;    
  }
  else if(mode==2){
    neopixel_nfc[idx][0] = red;
    neopixel_nfc[idx][1] = green;
    neopixel_nfc[idx][2] = blue;    
  }
  else if(mode==3){
    neopixel_http[idx][0] = red;
    neopixel_http[idx][1] = green;
    neopixel_http[idx][2] = blue;    
  }
}

void neopixel_update(){
  for(int i=0; i < NUMPIXELS; i++){
    if(mode==0) pixels.setPixelColor(i, pixels.Color(neopixel_blink[i][0],neopixel_blink[i][1],neopixel_blink[i][2]));
    else if(mode==1) pixels.setPixelColor(i, pixels.Color(neopixel_acc[i][0],neopixel_acc[i][1],neopixel_acc[i][2]));
    else if(mode==2) pixels.setPixelColor(i, pixels.Color(neopixel_nfc[i][0],neopixel_nfc[i][1],neopixel_nfc[i][2]));
    else if(mode==3) pixels.setPixelColor(i, pixels.Color(neopixel_http[i][0],neopixel_http[i][1],neopixel_http[i][2]));
  }
  pixels.show();  
}
