/*******************************/
/*          CONNECTION         */
/*******************************/
cubo_connected = false;

var HttpClient = function() {
    this.get = function(aUrl, aCallback) {
        var anHttpRequest = new XMLHttpRequest();
        anHttpRequest.onreadystatechange = function() {
            if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200)
                aCallback(anHttpRequest.responseText);
        }
        anHttpRequest.open( "GET", aUrl, true );
        anHttpRequest.send( null );
    }
};

client = new HttpClient();
function connect_cubo(){
    console.log("Connecting to Cubo.");
    console.log("Trying IP: "+ip);
    client.get(ip+":5000/ping", function(response){
        console.log("Connected")
    })

}

/*******************************/
/*          ANIMATION          */
/*******************************/
var cubex = -22,    // initial rotation
cubey = -38,
cubez = 0;
function rotate(variableName, degrees) {
    window[variableName] = window[variableName] + degrees;
    rotCube(cubex, cubey, cubez);
}
function rotCube(degx, degy, degz){
    segs = "rotateX("+degx+"deg) rotateY("+degy+"deg) rotateZ("+degz+"deg) translateX(0) translateY(0) translateZ(0)";
    document.getElementById('D3Cube').style.transform = segs;
}
function turnRight() {
    rotate("cubey", 90);
}
function turnLeft() {
    rotate("cubey", -90);
}
function flipCube() {
    rotate("cubez", -180);
}

function toggle_light(side){
    handle = document.getElementById(side);
    color = handle.style.backgroundColor;
    console.log("Clicked "+side+", which is "+handle.style.backgroundColor);

    if(handle.style.backgroundColor=='white' || handle.style.backgroundColor==''){
        handle.style.backgroundColor='yellow';
    }
    else{
        handle.style.backgroundColor='white';
    }
}