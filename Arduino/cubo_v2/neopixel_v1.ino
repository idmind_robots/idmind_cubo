void neopixel_start(){
  for(int i=0; i<NUMPIXELS;i++)
    pixels.setPixelColor(i, pixels.Color(0,0,255));
  pixels.show();
  delay(1000);
}

void neopixel_set_alarm(){
  for(int i=0; i<NUMPIXELS;i++)
    pixels.setPixelColor(i, pixels.Color(255,0,0));
  pixels.show();
//  delay(1000);
}

void neopixel_green_light(int idx){
  pixels.setPixelColor(idx, pixels.Color(0,255,0));
  pixels.show();
//  delay(10);
}

void neopixel_red_light(int idx){
  pixels.setPixelColor(idx, pixels.Color(255,0,0));
  pixels.show();
//  delay(10);
}

void neopixel_blue_light(int idx){
  pixels.setPixelColor(idx, pixels.Color(0,0,255));
  pixels.show();
//  delay(10);
}
