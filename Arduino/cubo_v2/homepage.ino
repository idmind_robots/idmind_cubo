String GetHomepage(){ //this serves the page
  String Header;
  Header =  "HTTP/1.1 200 OK\r\n";            //bog standard stuff - should provide alternative headers
  Header += "Content-Type: text/html\r\n";
  Header += "Connection: close\r\n";    

  String Content;
  Content = "<!DOCTYPE html>";
  Content += "<html lang=\"en\">";
  Content += "<head>";
  Content += "<link rel=\"stylesheet\" type=\"text/css\" href=\"static/css/cubo.css\">";
  Content += "<script src=\"static/js/cubo.js\"></script>"; 
  Content += "<title> ESP8266 test </title>";
  Content += "<body>";  
  Content += "<H1>Hello World</H1>";
  Content += "<div id=\"wrapD3Cube\"><div id=\"D3Cube\">";
  Content += "<div id=\"side1\" onclick=\"toggle_light(this.id)\"></div>";
  Content += "<div id=\"side2\" onclick=\"toggle_light(this.id)\"></div>";
  Content += "<div id=\"side3\" onclick=\"toggle_light(this.id)\"></div>";
  Content += "<div id=\"side4\" onclick=\"toggle_light(this.id)\"></div>";
  Content += "<div id=\"side5\" onclick=\"toggle_light(this.id)\"></div>";
  Content += "<div id=\"side6\" onclick=\"toggle_light(this.id)\"></div>";
  Content += "</div></div>";
  Content += "<p style=\"text-align: center;\"><a onclick=\"turnLeft()\">Left</a><a onclick=\"turnRight()\">Right</a> <br /><a onclick=\"flipCube()\">Flip</a></p>";
  Content += "</body></html>";

  Header += "Content-Length: ";  //ESP likes to know the length
  Header += (int)(Content.length());  //length determined here
  Header += "\r\n\r\n";    //blank line

  return Header+Content;
  
}

String GetJS(){
  String Header;
  Header =  "HTTP/1.1 200 OK\r\n";            //bog standard stuff - should provide alternative headers
  Header += "Content-Type: text/css\r\n";
  Header += "Connection: close\r\n";    
  
  String js = "";
  js += "cubo_connected = false;";
  js += "var HttpClient = function() { this.get = function(aUrl, aCallback) { var anHttpRequest = new XMLHttpRequest(); ";
  js += "anHttpRequest.onreadystatechange = function() { if (anHttpRequest.readyState == 4 && anHttpRequest.status == 200) ";
  js += "aCallback(anHttpRequest.responseText); } anHttpRequest.open( \"GET\", aUrl, true ); anHttpRequest.send( null ); }};";
        
  js += "client = new HttpClient();";
  js += "var cubex = -22, cubey = -38, cubez = 0";
  js += "function rotate(variableName, degrees) { window[variableName] = window[variableName] + degrees; rotCube(cubex, cubey, cubez);}";
  js += "function rotCube(degx, degy, degz){ segs = \"rotateX(\"+degx+\"deg) rotateY(\"+degy+\"deg) rotateZ(\"+degz+\"deg) translateX(0) translateY(0) translateZ(0)\";document.getElementById(\"D3Cube\").style.transform = segs;}";
  js += "function turnRight() {rotate(\"cubey\", 90);}";
  js += "function turnLeft() {rotate(\"cubey\", -90);}";
  js += "function flipCube() {rotate(\"cubez\", -180);}";
  js += "function toggle_light(side){handle = document.getElementById(side); color = handle.style.backgroundColor; console.log(\"Clicked \"+side+\", which is \"+handle.style.backgroundColor);if(handle.style.backgroundColor==\"white\" || handle.style.backgroundColor==\"\"){handle.style.backgroundColor=\"yellow\";} else{handle.style.backgroundColor=\"white\";}}";  

  Header += "Content-Length: ";  //ESP likes to know the length
  Header += (int)(js.length());  //length determined here
  Header += "\r\n\r\n";    //blank line

  return Header+js;  
}

String GetCSS(){
  String Header;
  Header =  "HTTP/1.1 200 OK\r\n";            //bog standard stuff - should provide alternative headers
  Header += "Content-Type: text/css\r\n";
  Header += "Connection: close\r\n";    
  
  String css = "";
  css += "#wrapD3Cube { width: 500px; height: 426px; margin: 20px auto; background-color: #EEE;}\n";
  css += "#D3Cube { width: 224px; height: 224px; top: 100px; transform-style: preserve-3d; -moz-transform-style: preserve-3d; -webkit-transform-style: preserve-3d; transform: rotateX(-22deg) rotateY(-38deg) rotateZ(0deg); -moz-transform: rotateX(-22deg) rotateY(-38deg) rotateZ(0deg); -webkit-transform: rotateX(-22deg) rotateY(-38deg) rotateZ(0deg); margin: auto; position: relative; -moz-transform-style: preserve-3d; transform-style: preserve-3d; -webkit-transition: all 0.5s ease-in-out; transition: all 0.5s ease-in-out;}\n";
  css += "#D3Cube > div { position: absolute; -webkit-transition: all 0.5s ease-in-out; transition: all 0.5s ease-in-out; width: 224px; height: 224px; float: left; overflow: hidden; opacity: 0.85; border: 1px solid black;}\n";
  css += "#side1 { transform: rotatex(90deg) translateX(0px) translateY(0px) translateZ(112px); -moz-transform: rotatex(90deg) translateX(0px) translateY(0px) translateZ(112px); -webkit-transform: rotatex(90deg) translateX(0px) translateY(0px) translateZ(112px); background: lime url() no-repeat center center;}\n"; // Mao
  css += "#side2 { transform: rotateY(-90deg) translateX(0px) translateY(0px) translateZ(112px); -moz-transform: rotateY(-90deg) translateX(0px) translateY(0px) translateZ(112px); -webkit-transform: rotateY(-90deg) translateX(0px) translateY(0px) translateZ(112px); background: pink url() no-repeat center center;}\n"; // Estrela
  css += "#side3 { transform: translateX(0px) translateY(0px) translateZ(112px); -moz-transform: translateX(0px) translateY(0px) translateZ(112px); -webkit-transform: translateX(0px) translateY(0px) translateZ(112px); background: red url() no-repeat center center;}"; //Coracao
  css += "#side4 { transform: rotateY(90deg) translateX(0px) translateY(0px) translateZ(112px); -moz-transform: rotateY(90deg) translateX(0px) translateY(0px) translateZ(112px); -webkit-transform: rotateY(90deg) translateX(0px) translateY(0px) translateZ(112px); background: yellow url() no-repeat center center;}"; //Raio
  css += "#side5 { transform: rotateY(180deg) translateX(0px) translateY(0px) translateZ(112px); -moz-transform: rotateY(180deg) translateX(0px) translateY(0px) translateZ(112px); -webkit-transform: rotateY(180deg) translateX(0px) translateY(0px) translateZ(112px); background: orange url() no-repeat center center;}"; //Lampada
  css += "#side6 { transform: rotateX(-90deg) translateX(0px) translateY(0px) translateZ(112px); -moz-transform: rotateX(-90deg) translateX(0px) translateY(0px) translateZ(112px); -webkit-transform: rotateX(-90deg) translateX(0px) translateY(0px) translateZ(112px); background-color: #FFF;}";

  Header += "Content-Length: ";  //ESP likes to know the length
  Header += (int)(css.length());  //length determined here
  Header += "\r\n\r\n";    //blank line

  return Header+css;
}

String Get404(){
  String Header;
  Header =  "HTTP/1.1 404 Not Found\r\n";            //bog standard stuff - should provide alternative headers
  Header += "Content-Type: text/css\r\n";
  Header += "Connection: close\r\n";    

  Header += "Content-Length: 0";  //ESP likes to know the length
//  Header += (int)(css.length());  //length determined here
  Header += "\r\n\r\n";    //blank line

  return Header;

}
