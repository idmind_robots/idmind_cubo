/**********************************************
 * IDMind's Library for the ESP8266 
 **********************************************/
 /*********************************************
  * Auxiliary Functions - Read/Write
  *********************************************/
String ReadCharacters(int total){
  bool debug = false;
  String esp_chars = "";
  int counter = 0;
  while(counter < total){
    if(Serial1.available() > 0){
      char c = Serial1.read();
      if(debug) Serial.print(c);
      esp_chars += c;
      counter++; 
    }
  }
  return esp_chars;    
}

String ReadLine(){
  bool debug = false;
  String line_buffer;
  while(true){
    if(Serial1.available() > 0){
      char c = Serial1.read();
      line_buffer += c;      
    }
    if(line_buffer.endsWith("\r\n")){
      if(debug) Serial.print(line_buffer);
      return line_buffer;
    }
  }
}

String SendCommand(String req){
  bool debug = false;
  String line = "";
  String reply = "";
  Serial1.print(req+"\r\n");
  line = ReadLine();
  if(line.startsWith(req) and line.endsWith("\r\n")){
    if(debug) Serial.println("Echo OK");
  }
  else{
    if(debug) Serial.println("Wrong Reply - "+line+"||");  
    delay(100);  
    while(Serial1.available() > 0) Serial1.read();
    return reply;
  }
  while(true){
    line = ReadLine();
    reply += line;
    if(line == "OK\r\n" or line == "ERROR\r\n")
      return reply;             
  }
}

/***************************************
 * Commands and reply handling
 **************************************/
/***************************************
 * Set as Access Point
 * Q: AT+CWMODE? R: +CWMODE:2
 * If needed: AT+CWMODE=2
 **************************************/
bool SetAPMode(){
  String reply = SendCommand("AT+CWMODE?");  
  if(int(reply[8])-48==2) return true;
  else{
    reply = SendCommand("AT+CWMODE=2");
    if(reply.endsWith("OK\r\n"))return true;
    else return false;
  }
}

/***************************************
 * Configure Wifi
 * AT+CWSAP=<ssid>,<pass>,<channel>,<security>, where
 * Security can be 0 - None, 1 - WEP, 2 - WPA_PSK, 3 - WPA2_PSK, 4 - WPA_WPA2_PSK
 **************************************/
bool SetWiFi(String ssid, String pass, int channel, int security){
  String reply = SendCommand("AT+CWSAP?");    
  int ssid_idx = 8;
  int ssid_idx2 = ssid_idx + ssid.length();
//  Serial.println(reply.substring(ssid_idx, ssid_idx2));
  int pass_idx = ssid_idx2 + 3;
  int pass_idx2 = pass_idx + pass.length();
//  Serial.println(reply.substring(pass_idx, pass_idx2));
  int chan_idx = pass_idx2 + 2;
  int chan_idx2 = chan_idx + String(channel).length();
//  Serial.println(reply.substring(chan_idx, chan_idx2));
  int sec_idx = chan_idx2 + 1;  
  if(reply.substring(ssid_idx, ssid_idx2)==ssid and reply.substring(pass_idx, pass_idx2)==pass and reply.substring(chan_idx,chan_idx2)==String(channel) and (reply[sec_idx]-48)==security){
    return true;
  }
  else{
    reply = SendCommand("AT+CWSAP=\""+ssid+"\",\""+pass+"\","+String(channel)+","+String(security));
    if(reply.endsWith("OK\r\n"))return true;
    else return false;    
  }
}

/***************************************
 * Set Multi Channel Connection
 * AT+CIPMUX=1
 **************************************/
bool SetMultiConnection(){
  String reply = SendCommand("AT+CIPMUX?");      
  if(int(reply[8])-48 == 1){
    return true;
  }
  else{
    reply = SendCommand("AT+CIPMUX=1");
    if(reply.endsWith("OK\r\n"))return true;
    else return false;    
  }
}

/***************************************
 * Restart Socket Server
 * AT+CIPSERVER=1
 **************************************/
bool StartServer(){
  String reply = SendCommand("AT+CIPSERVER=0");    
  if(reply.endsWith("OK\r\n")){
    reply = SendCommand("AT+CIPSERVER=1,80");  
    if(reply.endsWith("OK\r\n"))return true;
    else return false;  
  }
  else
    return false;
}

/***************************************
 *  Complex functions
 ***************************************/
bool EspStart(){
  /* Set as Access Point */
  while(!SetAPMode()){
    Serial.println("Failed to set as Access Point");
    neopixel_red_light(0);
  }
  Serial.println("Acess Point Mode activated");
  neopixel_green_light(0);
  delay(10);

  /* Configure the WiFi Network */
  while(!SetWiFi("Cubo", "asdf", 11, 0)){
    Serial.println("Failed to set WiFi");
    neopixel_red_light(1);
  }
  Serial.println("WiFi configured");
  neopixel_green_light(1);
  delay(10);
  
  /* Set Multi-Channel Connection */
  while(!SetMultiConnection()){
    Serial.println("Failed to set Multi Channel Connections");
    neopixel_red_light(2);
  }
  Serial.println("Multi Channel Connection activated");
  neopixel_green_light(2);
  delay(10);

  /* Start Server */
  while(!StartServer()){
    Serial.println("Failed to start Socket Server");
    neopixel_red_light(3);
  }
  Serial.println("Socket Server started");
  neopixel_green_light(3);
  delay(10);
    
  return true;
}

/********************************************************
 * Sends data in 'content' to channel 'ch_id'
 * There is a maximum size that can be sent, so the
 * content may be cut in chunks
 */
void SendData(int ch_id, String content){
  bool debug = true;  
  if(debug) Serial.println("Sending data to ESP - "+String(content.length()));
  String curr_substring;
  int idx = 0;
  for(unsigned int sub_idx=0; sub_idx < content.length(); sub_idx+=MAX_ESP_SIZE){
    idx += 1;
    if(sub_idx + MAX_ESP_SIZE > content.length()) curr_substring = content.substring(sub_idx);
    else curr_substring = content.substring(sub_idx, sub_idx+MAX_ESP_SIZE);
    Serial.println("Sending part "+String(idx)+": "+String(curr_substring.length()));
    Serial1.print("AT+CIPSEND=");    //send the web page
    Serial1.print(ch_id);
    Serial1.print(",");
    Serial1.println(curr_substring.length());    
    if (Serial1.find(">")) {  //prompt from ESP8266 indicating ready
      Serial1.print(curr_substring);  
      Serial.println("out it goes!!");
//      delay(10);
      /* Actually wait for response... */
//      Serial.print(ReadLine());
//      Serial.print(ReadLine());
//      Serial.print(ReadLine());
//      Serial.print(ReadLine());      
      while(!ReadLine().endsWith("SEND OK\r\n"));      
    }   
  }
  Serial.println("All sent");
}

/***********************************************************
 * Server Responses 
 * - TODO: Everything
 * Format: +IPD,<channel>,<length>,<format>
 * +IPD,0,422:GET / HTTP/1.1
 * Received Request
 * Host: 192.168.4.1
 * Connection: keep-alive
 * DNT: 1
 * Upgrade-Insecure-Requests: 1
 * User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.75 Safari/537.36
 * Accept: text/html,application/xhtml+xml,...
 * Accept-Encoding: gzip, deflate
 * Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
 ***********************************************************/
void HttpServer(){
  while(Serial1.available() > 0){
    char c = Serial1.read();
    esp_buffer += c;     
    if(esp_buffer.endsWith("CONNECT\r\n")){
      Serial.print("Started connection ");
      Serial.println(esp_buffer[0]);
      esp_buffer = "";
    }
    else if(esp_buffer.endsWith("CLOSED\r\n")){      
      Serial.print("Closed connection ");
      Serial.println(esp_buffer[esp_buffer.length()-10]);
      esp_buffer = "";
    }
    else if(esp_buffer.endsWith("+IPD")){      
      while(Serial1.available() <= 0);
      Serial1.read(); // Read Comma      
      while(Serial1.available() <= 0);
      int connection_id = Serial1.read()-'0';
      while(Serial1.available() <= 0);
      Serial1.read(); // Read Comma
      while(Serial1.available() <= 0);
      int req_length = (Serial1.readStringUntil(':')).toInt();
      
      String req = ReadCharacters(req_length);      
      char req_char[req.length()];
      req.toCharArray(req_char, req.length());
//      Serial.print(req_char);
      // Parse the request
      strtok(req_char, " ");
      char *token = strtok(NULL, " ");      
      Serial.println("Requested "+ String(token));
      if(strcmp(token, "/") == 0){
        Serial.println("Sending homepage");
        SendData(connection_id, GetHomepage());
      }
      else if(strcmp(token, "/static/css/cubo.css") == 0){        
//        SendData(connection_id, Get404());
        SendData(connection_id, GetCSS());
      }
      else if(strcmp(token, "/static/js/cubo.js") == 0){        
        SendData(connection_id, GetJS());
      }
      else{
        Serial.println("Not Found");
        SendData(connection_id, Get404());
      }
      esp_buffer = "";
    }
    else if(esp_buffer.endsWith("SEND OK\r\n")){
      Serial.println("Successfully Sent");      
      esp_buffer = "";
    }
    Serial.println(esp_buffer);
  }
}
