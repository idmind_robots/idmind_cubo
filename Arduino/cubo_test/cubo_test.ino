/********************************************
   Author: Carlos Neves
   Project: IDMind's Cubo
   Goals:
   1. Set up a WiFi Access Point
   2. Set up a HTTP Server
   3. Control the lights using the webpage
*/

#include <Adafruit_MMA8451.h>
#include <Adafruit_Sensor.h>
#include <PN532_HSU.h>
#include <PN532.h>
#include <NfcAdapter.h>
#include <Adafruit_NeoPixel.h>


/* NEOPixel Variables */
// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN_NeoPixel            6
#define PIN_PushButton          5
#define PIN_Vibrator            2

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      8

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN_NeoPixel, NEO_GRB + NEO_KHZ800);
int delayval = 500; // delay for half a second
int buttonState;             // the current reading from the input pin
int neoPixel_State = 0;
int reading = 0;
int blink_counter = 0;
unsigned long timeout = 10000;

int neopixel_blink[8][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
int neopixel_acc[8][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
int neopixel_nfc[8][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
int neopixel_http[8][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}, {0, 0, 0}};

int side2idx[6][4] = {
  // Old Cube
  //{5, 4, 7, 6}, // Hand
  //{0, 1, 5, 6}, // Star
  //{0, 3, 5, 4}, // Heart  
  //{2, 3, 4, 7}, // Lightning
  //{1, 2, 7, 6}, // Bulb
  //{0, 1, 2, 3}, // Empty
  {4, 5, 6, 7}, // Hand
  {0, 1, 5, 6}, // Star
  {0, 3, 4, 6}, // Heart  
  {2, 3, 4, 7}, // Lightning
  {1, 2, 6, 7}, // Bulb
  {0, 1, 2, 3}, // Empty
};

/* ESP Global Variables */
bool connections[4] = {false, false, false, false};
String esp_buffer = "";
String request = "";
int MAX_ESP_SIZE = 1750;

/* Accelerometer MMA8451 */
Adafruit_MMA8451 mma = Adafruit_MMA8451();
float ACC_MAX = 8191;
float v_acc[3] = {0.0, 0.0, 0.0};
float led_angles[8][3] = {
  {0.785, -0.785, 0.785},
  {-0.785, -0.785, 0.785},
  {-0.785, 0.785, 0.785},
  {0.785, 0.785, 0.785},  
  {0.785, 0.785, -0.785},
  {0.785, -0.785, -0.785},  
  {-0.785, -0.785, -0.785},
  {-0.785, 0.785, -0.785},
};


/* NFC Device */
PN532_HSU pn532hsu(Serial2);
NfcAdapter nfc = NfcAdapter(pn532hsu);
byte uid[4] = { 0x00, 0xFF, 0xAA, 0x17 };
byte uidFromTag[sizeof(uid)]; 

/* Mode variable
 *  0 - All lights in rainbow
 *  1 - NFC
 *  2 - Inclination
 *  3 - WebServer
 */
int mode = 0;

void setup()
{
  delay(5000);
  // Start NEOPixels
  pixels.begin();               // Initialize NeoPixel Library
  pinMode(PIN_PushButton, INPUT);
  pinMode(PIN_Vibrator, OUTPUT);
  neopixel_start();

  // Communication to PC
  Serial.begin(115200);

  // Communication to ESP8266
  Serial1.begin(115200);
  //  Serial1.setTimeout(10000);

  while (!EspStart());
  delay(100);
  while (Serial1.available()) Serial1.read();
  
  // Accelerometer
  while(!mma.begin()) {
    Serial.println("Couldnt start");
    delay(100);
  }
  mma.setRange(MMA8451_RANGE_2_G);  
  neopixel_green_light(4);
  Serial.println("MMA8451 found!");    
  delay(100);

  // NFC
  nfc.begin();
  neopixel_green_light(5);
  
  delay(1000);
}

void loop() {
  
  reading = digitalRead(PIN_PushButton);
  if(reading == HIGH){
    while(digitalRead(PIN_PushButton)==HIGH);
    if(mode < 7) mode++;
    else mode = 0;
    digitalWrite(PIN_Vibrator, HIGH);
    delay(100);
    digitalWrite(PIN_Vibrator, LOW);
    Serial.println("Changing to mode "+String(mode));    
  }
  for(int i=0; i<=mode;i++){
    pixels.setPixelColor(i, pixels.Color(255,0,0));
  }
  for(int i=mode+1; i<NUMPIXELS;i++){
    pixels.setPixelColor(i, pixels.Color(0,0,0));
  }
  pixels.show();
  
  // Hardware update
  // neopixel_update();
  
}
