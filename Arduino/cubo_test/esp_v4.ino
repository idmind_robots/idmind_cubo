/**********************************************
 * IDMind's Library for the ESP8266 
 **********************************************/
 /*********************************************
  * Auxiliary Functions - Read/Write
  *********************************************/
String ReadCharacters(int total){
  bool debug = false;
  String esp_chars = "";
  int counter = 0;
  while(counter < total){
    if(Serial1.available() > 0){
      char c = Serial1.read();
      if(debug) Serial.print(c);
      esp_chars += c;
      counter++; 
    }
  }
  return esp_chars;    
}

String ReadLine(){
  bool debug = false;
  String line_buffer;
  int timeout = 100000;
  int counter = 0;
  while(counter < timeout){
    if(Serial1.available() > 0){
      char c = Serial1.read();
      line_buffer += c;      
      counter = 0;
    }
    else
      counter++;
    if(line_buffer.endsWith("\r\n")){
      if(debug) Serial.print(line_buffer);
      return line_buffer;
    }
  }
  Serial.println("TIMEOUT DETECTED");
  return "";
}

String SendCommand(String req){
  bool debug = false;
  String line = "";
  String reply = "";
  Serial1.print(req+"\r\n");
  line = ReadLine();
  if(line.startsWith(req) and line.endsWith("\r\n")){
    if(debug) Serial.println("Echo OK");
  }
  else{
    if(debug) Serial.println("Wrong Reply - "+line+"||");  
    delay(100);  
    while(Serial1.available() > 0) Serial1.read();
    return reply;
  }
  while(true){
    line = ReadLine();
    reply += line;
    if(line == "OK\r\n" or line == "ERROR\r\n")
      return reply;             
  }
}

/***************************************
 * Commands and reply handling
 **************************************/
/***************************************
 * Set as Access Point
 * Q: AT+CWMODE? R: +CWMODE:2
 * If needed: AT+CWMODE=2
 **************************************/
bool SetAPMode(){
  String reply = SendCommand("AT+CWMODE?");  
  if(int(reply[8])-48==2) return true;
  else{
    reply = SendCommand("AT+CWMODE=2");
    if(reply.endsWith("OK\r\n"))return true;
    else return false;
  }
}

/***************************************
 * Configure Wifi
 * AT+CWSAP=<ssid>,<pass>,<channel>,<security>, where
 * Security can be 0 - None, 1 - WEP, 2 - WPA_PSK, 3 - WPA2_PSK, 4 - WPA_WPA2_PSK
 **************************************/
bool SetWiFi(String ssid, String pass, int channel, int security){
  String reply = SendCommand("AT+CWSAP?");    
  int ssid_idx = 8;
  int ssid_idx2 = ssid_idx + ssid.length();
//  Serial.println(reply.substring(ssid_idx, ssid_idx2));
  int pass_idx = ssid_idx2 + 3;
  int pass_idx2 = pass_idx + pass.length();
//  Serial.println(reply.substring(pass_idx, pass_idx2));
  int chan_idx = pass_idx2 + 2;
  int chan_idx2 = chan_idx + String(channel).length();
//  Serial.println(reply.substring(chan_idx, chan_idx2));
  int sec_idx = chan_idx2 + 1;  
  if(reply.substring(ssid_idx, ssid_idx2)==ssid and reply.substring(pass_idx, pass_idx2)==pass and reply.substring(chan_idx,chan_idx2)==String(channel) and (reply[sec_idx]-48)==security){
    return true;
  }
  else{
    reply = SendCommand("AT+CWSAP=\""+ssid+"\",\""+pass+"\","+String(channel)+","+String(security));
    if(reply.endsWith("OK\r\n"))return true;
    else return false;    
  }
}

/***************************************
 * Set Multi Channel Connection
 * AT+CIPMUX=1
 **************************************/
bool SetMultiConnection(){
  String reply = SendCommand("AT+CIPMUX?");      
  if(int(reply[8])-48 == 1){
    return true;
  }
  else{
    reply = SendCommand("AT+CIPMUX=1");
    if(reply.endsWith("OK\r\n"))return true;
    else return false;    
  }
}

/***************************************
 * Restart Socket Server
 * AT+CIPSERVER=1
 **************************************/
bool StartServer(){
  String reply = SendCommand("AT+CIPSERVER=0");    
  if(reply.endsWith("OK\r\n")){
    reply = SendCommand("AT+CIPSERVER=1,80");  
    if(reply.endsWith("OK\r\n"))return true;
    else return false;  
  }
  else
    return false;
}

/***************************************
 *  Complex functions
 ***************************************/
bool EspStart(){
  /* Set as Access Point */
  while(!SetAPMode()){
    Serial.println("Failed to set as Access Point");
    neopixel_red_light(0);
  }
  Serial.println("Acess Point Mode activated");
  neopixel_green_light(0);
  delay(10);

  /* Configure the WiFi Network */
  while(!SetWiFi("Cubo", "asdf", 11, 0)){
    Serial.println("Failed to set WiFi");
    neopixel_red_light(1);
  }
  Serial.println("WiFi configured");
  neopixel_green_light(1);
  delay(10);
  
  /* Set Multi-Channel Connection */
  while(!SetMultiConnection()){
    Serial.println("Failed to set Multi Channel Connections");
    neopixel_red_light(2);
  }
  Serial.println("Multi Channel Connection activated");
  neopixel_green_light(2);
  delay(10);

  /* Start Server */
  while(!StartServer()){
    Serial.println("Failed to start Socket Server");
    neopixel_red_light(3);
  }
  Serial.println("Socket Server started");
  neopixel_green_light(3);
  delay(10);
    
  return true;
}

/********************************************************
 * Sends data in 'content' to channel 'ch_id'
 * There is a maximum size that can be sent, so the
 * content may be cut in chunks
 */
bool SendData(int ch_id, String content){
  bool debug = true;
  bool success = false;
  if(debug) Serial.println("Sending data to ESP - "+String(content.length()));
  String curr_substring;
  int idx = 1;
  for(unsigned int sub_idx=0; sub_idx < content.length(); sub_idx+=MAX_ESP_SIZE){
    success = false;
    while(!success){      
      if(sub_idx + MAX_ESP_SIZE > content.length()) curr_substring = content.substring(sub_idx);
      else curr_substring = content.substring(sub_idx, sub_idx+MAX_ESP_SIZE);
      Serial.println("Sending part "+String(idx)+": "+String(curr_substring.length()));
      Serial1.print("AT+CIPSEND=");    //send the web page      
      Serial1.print(ch_id);
      Serial1.print(",");
      Serial1.println(curr_substring.length());    
      int fails = 0;
      if (Serial1.find(">")) {  //prompt from ESP8266 indicating ready
        Serial1.print(curr_substring);  
        Serial.println("out it goes!!");
        /* Actually wait for response... */
        while(true){
          if(Serial1.available() > 0){
            String line = ReadLine();          
            if(line.endsWith("SEND OK\r\n")){
              idx += 1;
              success = true;
              break;
            }
            else if(line.endsWith("FAIL\r\n")){
              return false;
            }
            else if(line.length() == 0){
              // TIMEOUT - Don't react...
              success = true;
              break;
            }
          }
        }
      }
      else{
        fails ++;
        if(fails==3){
          Serial.println("Moving on");
          success = true;
          break;
        }
      }
    }   
  }
  Serial.println("All sent");
  return true;  
}

/***********************************************************
 * Server Responses 
 * - TODO: Everything
 * Format: +IPD,<channel>,<length>,<format>
 * +IPD,0,422:GET / HTTP/1.1
 * Received Request
 * Host: 192.168.4.1
 * Connection: keep-alive
 * DNT: 1
 * Upgrade-Insecure-Requests: 1
 * User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.75 Safari/537.36
 * Accept: text/html,application/xhtml+xml,...
 * Accept-Encoding: gzip, deflate
 * Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
 ***********************************************************/
void HandleConnections(){
  while(Serial1.available() > 0){
    char c = Serial1.read();
    esp_buffer += c;
    if(esp_buffer.endsWith("CONNECT\r\n")){
      int con_id = esp_buffer[esp_buffer.length()-11]-'0';
      Serial.println("Starting connection "+String(con_id));
      connections[con_id] = true;
      esp_buffer = "";      
    }
    else if(esp_buffer.endsWith("CLOSED\r\n")){
      int con_id = esp_buffer[esp_buffer.length()-10] -'0';
      Serial.println("Closed connection "+String(con_id));
      connections[con_id] = false;
      esp_buffer = "";
    }
    else if(esp_buffer.endsWith("+IPD")){
      while(Serial1.available() <= 0);
      Serial1.read(); // Read Comma      
      while(Serial1.available() <= 0);
      int connection_id = Serial1.read()-'0';
      while(Serial1.available() <= 0);
      Serial1.read(); // Read Comma
      while(Serial1.available() <= 0);
      int req_length = (Serial1.readStringUntil(':')).toInt();      
      request += String(connection_id)+","+ReadCharacters(req_length)+"||";
      esp_buffer = "";
    }
  }
}

void HandleRequests(){  
  if(request.length() > 0){
    Serial.println(request);
    int idx = 0;
    int ch_id = request[0]-'0';
    String curr_request = "";
    // Parse the oldest request
    while(!curr_request.endsWith("||")){
      curr_request += request[idx];
      idx++;
    }
    // Remove from request list
    request = request.substring(curr_request.length());

    //Handle request
    char req_char[curr_request.length()];
    curr_request.toCharArray(req_char, curr_request.length());
    // Parse the request
    strtok(req_char, " ");
    char *token = strtok(NULL, " ");    
    Serial.println("Requested "+ String(token));    
    if(strcmp(token, "/") == 0){
      Serial.println("Sending homepage");      
      SendData(ch_id, GetHomepage());
    }
    else{
      char *action = strtok(token, "/");
      Serial.println(action);
      if(strcmp(action, "static")==0){        
        strtok(NULL, "/"); // img
        char *img = strtok(NULL, "/");
        Serial.println("Sending a picture (under construction" + String(img));
      }
      else if(strcmp(action, "color")==0){        
        char *side = strtok(NULL, "/");
        char *color = strtok(NULL, "/");  
        Serial.println("Changing color of "+String(side)+" to "+String(color));
        for(int idx=0; idx<4; idx++){
          neopixel_assign(side2idx[int(side[4]-'0')-1][idx], hex_to_int(color[0])*16+hex_to_int(color[1]), hex_to_int(color[2])*16+hex_to_int(color[3]), hex_to_int(color[4])*16+hex_to_int(color[5]));
        }
        SendData(ch_id, Get200());
      }
//      if(token == "/static/img/mao.png"){
//        Serial.println("Sending hand picture");
//        SendData(ch_id, GetImage("hand"));
//      }
//      else if(token == "/static/img/estrela.png"){
//        Serial.println("Sending star picture");
//        SendData(ch_id, GetImage("star"));
//      }
//      else if(token == "/static/img/lampada.png"){
//        Serial.println("Sending hand picture");
//        SendData(ch_id, GetImage("bulb"));
//      }
//      else if(token == "/static/img/relampago.png"){
//        Serial.println("Sending lightning picture");
//        SendData(ch_id, GetImage("lightning"));
//      }
//      else if(token == "/static/img/coracao.png"){
//        Serial.println("Sending heart picture");
//        SendData(ch_id, GetImage("heart"));
//      }
//      else if(token"side")){
//        Serial.println("Request to color side");
//        SendData(ch_id, GetImage("heart"));
//      }
//      else{
//        Serial.println("Unrecognized request - "+String(token));
//        SendData(ch_id, Get404());      
//      }      
    }
  }
}

int hex_to_int(char c){
  int first;
  int sec;
  int value;
  
  if (c >= 97) {
    c -= 32;
  }
  first = c / 16 - 3;
  sec = c % 16;
  value = first * 10 + sec;
  if (value > 9) {
    value--;
  }
  return value;
}
