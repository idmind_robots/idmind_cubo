String GetHomepage(){ //this serves the page
  String Header;
  Header =  "HTTP/1.1 200 OK\r\n";            //bog standard stuff - should provide alternative headers
  Header += "Content-Type: text/html\r\n";
  Header += "Connection: close\r\n";    

  String Content;
  Content = "<!DOCTYPE html>";
  Content += "<html lang=\"en\">";
  Content += "<head>";
  Content += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">";
  Content += ServeCSS();
  Content += ServeJS();
  Content += "<title>Cubo Interface</title>";
  Content += "<body>";  
  Content += "<H1>Cubo v1.0</H1><div id=\"color_menu\">Choose a color and click my sides!<input id=\"color_picker\" type=\"color\" name=\"favcolor\" value=\"#ff0000\"></div>";  
  Content += "<div id=\"wrapD3Cube\"><div id=\"D3Cube\">";
  Content += "<div id=\"side1\" onclick=\"toggle_light(this.id)\"></div>";
  Content += "<div id=\"side2\" onclick=\"toggle_light(this.id)\"></div>";
  Content += "<div id=\"side3\" onclick=\"toggle_light(this.id)\"></div>";
  Content += "<div id=\"side4\" onclick=\"toggle_light(this.id)\"></div>";
  Content += "<div id=\"side5\" onclick=\"toggle_light(this.id)\"></div>";
  Content += "<div id=\"side6\" onclick=\"toggle_light(this.id)\"></div>";
  Content += "</div></div>";  
  Content += "<div id=\"cube_control\"><input type=\"button\" value=\"Turn Left\" onclick=\"turnLeft()\">";
  Content += "<input type=\"button\" value=\"Turn Right\" onclick=\"turnRight()\">";
  Content += "<input type=\"button\" value=\"Flip\" onclick=\"flipCube()\"></div>";
  Content += "</body></html>";

  Header += "Content-Length: ";  //ESP likes to know the length
  Header += (int)(Content.length());  //length determined here
  Header += "\r\n\r\n";    //blank line

  return Header+Content;
  
}

String Get404(){
  String Header;
  Header =  "HTTP/1.1 404 Not Found\r\n";            //bog standard stuff - should provide alternative headers
  Header += "Content-Type: text/css\r\n";
  Header += "Connection: close\r\n";    

  Header += "Content-Length: 0";  //ESP likes to know the length
  Header += "\r\n\r\n";    //blank line

  return Header;
}

String Get200(){
  String Header;
  Header =  "HTTP/1.1 200 OK\r\n";            //bog standard stuff - should provide alternative headers
  Header += "Content-Type: text/css\r\n";
  Header += "Connection: close\r\n";    

  Header += "Content-Length: 0";  //ESP likes to know the length
  Header += "\r\n\r\n";    //blank line

  return Header;  
}

String GetJS(){
  String js = "cubo_connected=!1;var HttpClient=function(){this.get=function(t,e){var n=new XMLHttpRequest;n.onreadystatechange=function(){4==n.readyState&&200==n.status&&e(n.responseText)},n.open(\"GET\",t,!0),n.send(null)}};client=new HttpClient;var cubex=-22,cubey=-38,cubez=0;function rotate(t,e){window[t]=window[t]+e,rotCube(cubex,cubey,cubez)}function rotCube(t,e,n){segs=\"rotateX(\"+t+\"deg) rotateY(\"+e+\"deg) rotateZ(\"+n+\"deg) translateX(0) translateY(0) translateZ(0)\",document.getElementById(\"D3Cube\").style.transform=segs}function turnRight(){rotate(\"cubey\",90)}function turnLeft(){rotate(\"cubey\",-90)}function flipCube(){rotate(\"cubez\",-180)}function toggle_light(t){handle=document.getElementById(t),color=document.getElementById(\"color_picker\").value,console.log(\"Clicked \"+t+\" to turn to \"+color.substring(1,7)),client.get(\"/color/\"+t+\"/\"+color.substring(1,7),function(t){handle.style.backgroundColor=color})}";
  return js;
}

String ServeJS(){ 
  String js = "<script>";
  js += GetJS();
  js += "</script>";
  return js;  
}

String GetCSS(){
  String css = "#body{display:flex;flex-direction:column;align-items:center}";
  css += "#h1{font-size:3vw}";
  css += "#color_menu{flex:0 0 auto;text-align:center}#color_picker{margin:10px 20px}#cube_control{flex:0 0 auto;text-align:center}";  
  css += "#wrapD3Cube{flex:0 0 auto;width:500px;height:426px;margin:20px auto;background-color:#white;border:1px solid grey;border-radius:10px}";
  css += "#D3Cube{width:224px;height:224px;top:100px;transform-style:preserve-3d;-moz-transform-style:preserve-3d;-webkit-transform-style:preserve-3d;transform:rotateX(-22deg) rotateY(-38deg) rotateZ(0);-moz-transform:rotateX(-22deg) rotateY(-38deg) rotateZ(0);-webkit-transform:rotateX(-22deg) rotateY(-38deg) rotateZ(0);margin:auto;position:relative;-moz-transform-style:preserve-3d;transform-style:preserve-3d;-webkit-transition:all .5s ease-in-out;transition:all .5s ease-in-out}";
  css += "#D3Cube>div{position:absolute;-webkit-transition:all .5s ease-in-out;transition:all .5s ease-in-out;width:224px;height:224px;float:left;overflow:hidden;opacity:.85;border:1px solid #000}";
  css += "#side1{transform:rotatex(90deg) translateX(0) translateY(0) translateZ(112px);-moz-transform:rotatex(90deg) translateX(0) translateY(0) translateZ(112px);-webkit-transform:rotatex(90deg) translateX(0) translateY(0) translateZ(112px);background:#fff url("+ServeHand()+") no-repeat center center}";
  css += "#side2{transform:rotateY(-90deg) translateX(0) translateY(0) translateZ(112px);-moz-transform:rotateY(-90deg) translateX(0) translateY(0) translateZ(112px);-webkit-transform:rotateY(-90deg) translateX(0) translateY(0) translateZ(112px);background:#fff url() no-repeat center center}"; //Estrela
  css += "#side3{transform:translateX(0) translateY(0) translateZ(112px);-moz-transform:translateX(0) translateY(0) translateZ(112px);-webkit-transform:translateX(0) translateY(0) translateZ(112px);background:#fff url() no-repeat center center}"; // Coracao
  css += "#side4{transform:rotateY(90deg) translateX(0) translateY(0) translateZ(112px);-moz-transform:rotateY(90deg) translateX(0) translateY(0) translateZ(112px);-webkit-transform:rotateY(90deg) translateX(0) translateY(0) translateZ(112px);background:#fff url() no-repeat center center}"; //Relampago
  css += "#side5{transform:rotateY(180deg) translateX(0) translateY(0) translateZ(112px);-moz-transform:rotateY(180deg) translateX(0) translateY(0) translateZ(112px);-webkit-transform:rotateY(180deg) translateX(0) translateY(0) translateZ(112px);background:#fff url() no-repeat center center}"; //Lampada
  css += "#side6{transform:rotateX(-90deg) translateX(0) translateY(0) translateZ(112px);-moz-transform:rotateX(-90deg) translateX(0) translateY(0) translateZ(112px);-webkit-transform:rotateX(-90deg) translateX(0) translateY(0) translateZ(112px);background-color:#fff}";
  return css;
}
String ServeCSS(){
  String css = "<style>\n";
  css += GetCSS();
  css += "</style>";
  return css;
}

String GetImage(String img_name){
  String Header;
  Header =  "HTTP/1.1 200 OK\r\n";            //bog standard stuff - should provide alternative headers
  Header += "Content-Type: image/png\r\n";
  Header += "Connection: close\r\n";
  String Content = "";
  if(img_name=="hand") Content = ServeHand();
  else if(img_name=="heart") Content = ServeHeart();
  else if(img_name=="star") Content = ServeStar();
  else if(img_name=="bulb") Content = ServeBulb();
  else if(img_name=="lightning") Content = ServeLightning();
  Header += "Content-Length: ";  //ESP likes to know the length
  Header += (int)(Content.length());  //length determined here
  Header += "\r\n\r\n";    //blank line
  return Header+Content;
}

String ServeHeart(){
  String heart = "'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHAAAABhCAYAAAAOaiV1AAAIx0lEQVR4Xu1dPWwcRRR+7xCJrwu0UCQSlCCngTJORyocpPjWoYgt0SLiBtEZd4gmQbRIMQXx3kUipgpdTAlNDJQgxQ1tki6OkW/Qd7ebDJO925nd+bV2pAjke/v2zfvmvXnvzc8ydS1pDXDS0nfCUwdg4oOgA7ADMHENJC5+Z4EdgIlrIHHxOwvsAExcA4mL31lgB2DiGkhc/M4COwAT10Di4ncW2AEYlwbW1pbPHB4uLDKPzwhBi9XS9fbw94WFw/3t7d0ncfXATJrkLTDLsiWi8RKRuCAELzLTGRMVCEEAcI+Zfu/1xO7t23f2TZ4PTZskgKurK8tC8IdCiGVTwOoUDkCZeZeIv8/zfGKpMbdkAJy6xlPXiegaEZ31pNQDIrG1sPDvbqyuNnoAS+CEoM9sW5vuIJhaJX2T56MvdZ/xRRc1gKurg+vjsdgMBVwFCAfMtLGzM9r1BVDde6IE8OrVK4vHx3yLeVYUWdctt78LQbv9/tF6DG41OgCz7MqaEHwjIqurHA1Tt9q7HDrQiQrALBvcIhJrbu3HLndm3tjZGd60y1WfWxQAIlB5+vTU/VhdZr06eTvPh+v1dPYpggOYPnglKGFADArgyQEvHIjBADx54IUBMRiAKQYsujOYz8AmCIBI0IUQN3QVkiZd76KPFMM7gEjSx2N+kCYo+lIjT+z3j865Tva9AzgYrDxIN13QBxCUqNgMh6PLZk+ZUXsFMMtWUAzeNBMxdWq3rtQbgFmWnRViDOszWnBNHT4iOsjz0TlX/fAIYLsy2RtE9L4QhP/iX7/QyCMiwr8/mOnP4v9NlPUOEb1d8MVzbxU8wPMpEf3FTH8T0T8mTF+iFet5fme7FYsZD3sBENZHNH7YpANQ8EdC0OuaD0PZ9wqlz3vkPSK65IDvjHc6s0JPAJpbHwD7WIiJRVQ1WAjaLGBhjT8wT6xIbrDcT+bwrRsns/jWPUfkxgqdA1hUXB6azH1wkZ8K8dxNQjkADMr7lfkld1a6V1hV6VrxDNzetxKI+A18QV828MWOpio3icHzrhBUx7cevElEuj8cjs7r0JrQOAcQ63tEfEtXKBU8WNCPzPSbBgMABLd4QaIF6N8xT4CVwQNfuNpfNPiC5BIRfSDEc2p1cOiw6fXEedu73pwDaJL3QcmfS/NSEyVBkbAYuN+yYQC8WVgS/gbwYJmmgYk6uMrBoQMeaISgm8PhaEOXXofOKYCmwYs8yuHavq6Yw3Q6BRpYIYKfqgbw4DKbNICIQVY2Q17WgxnHAJq5z6+kec9QMZVYIFhBFCu3n5npXhPkpGfkgWZqhbbdqGMA9aNPKBoKR4Nrg/W1bXDJm9KggFVvOeD7hYGnsL1S0V5Lc7ScZSvI/bQ24cqjGnOWbnBRB7LsSm1Ydfk+zLGYa9FM+NqujzoDsNiQ+7hOwW0VosMf6QACF9OgZR5vecAZumWr86AzAItDJ/d1FAwahPhl0m4yonX526aDrJAZzRBAyvORNb1bY6QqyDT/kwOOFACUUxVTAInsrVA4BNBs6Uh2SUi8Ed3F3NrJewIBlEc0AhgEMjE32eUjYjabX+3VRZ1pyXTTkpwg20ojXA0ApCfIWdEQHCGNMGxbtk46Gb9ZV9Asu3KfiJd06UGHnK1cXUC+Vq44mPDwQSt7C9NEvpDvZAKI0ldZiEbxGstBMTbZfTacr+MHcDBYuctMyyYAwPpghaVrghWq63km/FzQyulDQ/cJseIHsOkGJrnCEWMwI1ufefowHVI2y2nOfFRTAGUrRGfNIzwXdjflKc99sL7mHiKBNKLN7mt5LowlIlUL45ifdRaZq4dTAgCaltLkjqrKauqqbNqiXCnCWiKqRU1bEqU002K2qgx5eQm/hSyvqYvDLd16GsVsKH0wWHlssplJBVEOaDDnQHG+c0N1Bb7tUlcyy0kFgMaphOpK5Y1ITffINHV1qitvmLSrr7eWQkwi2qad03muTSBT8m+7kUhHzioadRebrcGT2JaK5juyZaXKyTP+7qNKI+d7TXexqQMDR86Gw9FrTQdV1XNOLbBwo1aOk6lbBV0m+fLcazeAsn8RgnMAbbjRcuSpm2vb5WLVdqCCZ/cd9vK/UnrnALZNJ+ZFpvjNpoLdgufmmJlzAKFk07XBujnChaJd8JT7YbP++T++dcqy8buLc/GqwpvmZ4g2Ubortwjatmrwc3le3osF2sgJqwaSCqJpdFp1WsmmS5Zktpr7ebfAqRu1k1LUzYm6IPoCz6X1OU/kVWUPBiu4RhLXJlttppboC7xpJ+1tYAqSB8ovLSJS3BGjtd3eBGW14DyrclJ1/rDJUTM92cRent+5qEfbjMrbHFiK12aZqa6LarKvgugTvOJC2PN5nh/Uyd3md+8AFgGNE1cK3lUglpuj5GPbtspjs5TPTJd93K0dBMBpUGO+7VB3pFZZ2ryz87p8delcnMSdOVB0hbJN5/q6yaqLEtAHW6sKs/Rhe72vTu/BLBCCNbnBoq5D8u/YIIWtEOWtFNgKgX2crrYq4iaKfv/oousL7uQ+BgUQghSfGMB92U6u4CorLY8sHK2eN3hCgAd5ggMoWWKyl56H/I5EFACWIB4evnrX9DyFiUt1Q2t/jc9EzmgAfJEnmp0rNOmsTdppnic2XF1ipytrdABOU4zJNwFxu5P1io2uYubTiT2iV9ZdJ+k6skYJ4AuXOvncXDQXxMLqej3eCvmlFhXUaAF84VKxiiE2Q3+SB8l5v3+05TNFSNoCVeGny1F+gSy/G0jU247BXVYBGr0FqkIj+X/27PTaeCyuubo8HWkBs/gpdIByoiywqjNTqzxeIupdIBI4zt0o6EESzsz7AO306aO92NzkPCCTs8B5nSk/QU50fJaI54LJTPtC9J74+DiHjiU1pTlRADZVQsrPdQCmjF4stdDEdRhU/M4Cg6q//cs7ANvrMCiHDsCg6m//8g7A9joMyqEDMKj627+8A7C9DoNy6AAMqv72L+8AbK/DoBz+A1lO1I8X8Q/DAAAAAElFTkSuQmCC'";
  heart = "";
  return heart;
}

String ServeStar(){
  String star = "'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHAAAABhCAYAAAAOaiV1AAAJWklEQVR4Xu2dT2xURRzHf7NNcJtoQvUCHpQqJCqXNpCgCQQbw6EHtT20+4oHuyf1BL0YOWFPGC/gST21HKRvtweqHnoghiWSKAkJvSAmKlQP0ovQA0lLDTvm+3YfzK67+2bem5n3pnlzKezOmz+/z/xmfvOb37xllCenJcCcbn3eeMoBOj4IcoA5QMcl4Hjzcw3MATouAcebn2tgDtBxCTje/FwDc4COS8Dx5ucamAN0XAKONz/XwBxgtiTged6bjRbVm39b28cYW+ecreBT3/dr2Wq9emuc1kDP8/YwxsfqdX6UMRoioj2qIuCcVhgD0PoVor6a7/urqmWkmd85gIDGef0EYzQWB1iUsAG0UGDnOWdLLsB0BuDU1OQY5/wEEes4NUaBifc9mydi57M81WYeYGNNq8+Z0DZ5qLxG1DebRZCZBYipkujRnF2Ni0LK5ovFhzPz80vrUTltfZ9JgJ43+SkRnbYlBJV6OKf1QoHKCwvVJZXnTOXNFMCmgXKxaVGa6rOWcjmnpf7+rXLa2pgZgMePTww9esQuM0Y7tUjYQiGwWPv6ePnChcVgX5lGygRAz5uYJmIwVJxLmFL7+vhIWhBTB+gyvHC0pQkxVYCm4R1+7XU6uG+Ynn92F93861e6evMn+mPNjKMlLYipATQN751DowSA7enr5bltBTEVgKYNloGnd9KpiZmA3cbWJt1/sB5oIRL+fWbxrLG1FprY3781aMs6tQ5wenps5+bmjhsmPSuTR8bp4F74tomqP16k67+v0PtvTdH+F15p+cwYReI1318cMVf+k5KtAyyVJrHPgyPaSBK1T9S2bp8baUSj0Fnfr8IhYTRZBdhwSNNFkz3qpH1hfb2+M9GmQoEPm95eWAOIqXNjY8cdkxv1KC2L+l43RGz0K5XqsO5yxfKsASyVJs8yRidNdkZGw8Q8Ji3SJ/3kZd9fnDfVbysAGycL9TumOoFyZbVLzHd7bZW+WjbrADJtlVoCWJoj4tMmAcpoX6e10I4WmjNojAPMkvaFALEnPPnuR8F/bWghEa36fnXQxAC2AND82Z7odQn3fVHC+nC0TC/tasRA2dFCM2uhDYBY+5SjxaIAhN/37yjSJxMzhL8qXpaXd+2hD0bL1rQQ54eVSnVctl+y+YwCNL3vAzSsfXE9LKIWymqurGA75ysM6o50MwrQ8/QYLwCFdQsW5MAzA8G/8Vk4BUJYKtoXClfUQnwGv+nde2t078F6UN7f/9ylza1Nbc5vxtjMwkLlXLJB0Pq0YYCTStMnBFoErOd2d4TUq+Pnf1gIjoxU07HhETo2FB2pCKAB1HtrAejbd+8Ef/F/2WRiGjUGUMX6hBBx9AOtkk0QXKgd13+7EQg3bsLA2f/iq8Gg2d3UbtmyUO9315alBg/2hJVKdUC2bJl8BgHKhUng1ADrWKcUjnocwm483GiZ3mQ6lyTP42l692AwsHrBhSae+/ZLqUGk2z9qDKCs6wz7sfCs7uovP9PNP28pT01JQMV5NpzqcdofGlCXVmp06cZlieL0bieMAfS8icsyQbmfl2etmfIS0lXOErYfZ46wZCWS1mMmgwDlDBi7B60S4lXIIrrvFDRQ62GvSYBcRhbtpryd/ZhMy3rnEeFhDfxs8Www9Ucnvaf1qQNEh9sNmaxDbIcHV5zsdkK3JWoEYDPu5X70aHySwwWIsEYx5YcOBGicCrywt75f1SZ3bQWJsJpXwmRMshbGWYYIePCdhhZzXHjo8LYF2Gk6xdYCG+U0k054rgBMdALfrokKJrp2zrrhOQEQjfS8SSkrtJvEswCxHR4MFfhck7jtnDBidADsNJ3a1MRO8GCwyG0Vek0E7mwjlE4iunUZRgOMh9DRLb9hTjabii4+aJ4eeGiTMwDlXGkyYhYhQgNOf3NG5rHYeUTngl54QZPccKXJOrNlpSzGvZiOYRHPCHXXpftQ18g+sLEGyh0nxQEIDUy+FnWvWTSgdAMkKozofF2JMYC4QlavM9xC0pLC+BXbU6juNVfnJh6CNQawaYlqMWSCheO9U4EhYymOk2IcE0UOVKdCKhoA9QU1ASCSLc9MaIXqHDC61z/jGqgrrFC0Cm2dVIjnlB/P6XrnkGNhhRghpdLk/aRXykxahd3mPbFOxLvIHhd1n0f17v/CeoyugU2Aia+Viedv+rSh95KFWBdoIVLckMXWGvTGwlgDqBJe2E2koQWqGrwLo+fA3iHqf6o/CNJViRsVL8AktUR1+z9FORnXQB3GjGrgE8Ad3v/G/2JNMQAQOQafqkwK6wV4aGGCpNX7kgLA+Bc8xQuZUZrQDVy74GWDcUNLFOsf1sE4aVtc8EyihaIF2mstgvfk7UOjLdHd2PRD43Df4cC+4cevHglBYIuA77u9vUnH2mti62BdA1Fh3JccRFmDAIc80NQwARz2i3i1luhyQx7kDd8hEwVSg/Vr7GKnNSNGHC1TU6WTnHOl1yR10wJYiXBwi+BQF6bZdnDtUx+eKR0Zb7ndhDxY6xC+ER7Yitofzyeq1+/ZaQq3YsSIFctGbIfPiFqATTyEi8/Eq2XIC8ME06HKaTkA9SpL1FZVgJzTuUql2njfl8FkHaDqVNoe+Nsuizjg2svoBlKckuUDd4lsvB8mlSk0rFT1pKLTHT4YIJWmRuoa4N3W0++vLUtvPWB1MlYY1n0Tt1sfrWtg2BDV80JsrHGHL1ir/rylwbXVHTvWV1wyxZU2rIuy03Ia7wxNDSDEpwpRl6aZK8eMu6xXe1MFuL0g2ocH+aUO0HWIzTVvXGeYhMoMkQmAaDDODut1mkt69KTSeQ15VwsFPm76lZKZnkLFxjVfxQyIjdftZjrxWrH477itVytnzgrtxUZ3SKLOcdD46R02q/t9L3HbmJkptL0DuKLGeR2HwRnSxuBXzMq29ngyUDMLsG2/iKAUY+9bixZU/vNz0TKKyIE9I+cMv9xpUSOzCy4UV+Y1sJ1rww1XOME5HzNksa4ifpOxwhdZmiqdMmJk1bXpUx0j4kc5Z0NxgMIoIaJaocCuMFavpbklkO23mM85DezVyUYAFeGXrYc45z1+xo6vEvWtFoubK2lvA+JA27YAkwrDxee3lQa6CCBpm3OASSWY8vM5wJQBJK0+B5hUgik/nwNMGUDS6nOASSWY8vM5wJQBJK0+B5hUgik/nwNMGUDS6nOASSWY8vP/AQEZO620uQnfAAAAAElFTkSuQmCC'";
  star = "";
  return star;
}

String ServeBulb(){
  String bulb = "'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHAAAABhCAYAAAAOaiV1AAAJWklEQVR4Xu2dT2xURRzHf7NNcJtoQvUCHpQqJCqXNpCgCQQbw6EHtT20+4oHuyf1BL0YOWFPGC/gST21HKRvtweqHnoghiWSKAkJvSAmKlQP0ovQA0lLDTvm+3YfzK67+2bem5n3pnlzKezOmz+/z/xmfvOb37xllCenJcCcbn3eeMoBOj4IcoA5QMcl4Hjzcw3MATouAcebn2tgDtBxCTje/FwDc4COS8Dx5ucamAN0XAKONz/XwBxgtiTged6bjRbVm39b28cYW+ecreBT3/dr2Wq9emuc1kDP8/YwxsfqdX6UMRoioj2qIuCcVhgD0PoVor6a7/urqmWkmd85gIDGef0EYzQWB1iUsAG0UGDnOWdLLsB0BuDU1OQY5/wEEes4NUaBifc9mydi57M81WYeYGNNq8+Z0DZ5qLxG1DebRZCZBYipkujRnF2Ni0LK5ovFhzPz80vrUTltfZ9JgJ43+SkRnbYlBJV6OKf1QoHKCwvVJZXnTOXNFMCmgXKxaVGa6rOWcjmnpf7+rXLa2pgZgMePTww9esQuM0Y7tUjYQiGwWPv6ePnChcVgX5lGygRAz5uYJmIwVJxLmFL7+vhIWhBTB+gyvHC0pQkxVYCm4R1+7XU6uG+Ynn92F93861e6evMn+mPNjKMlLYipATQN751DowSA7enr5bltBTEVgKYNloGnd9KpiZmA3cbWJt1/sB5oIRL+fWbxrLG1FprY3781aMs6tQ5wenps5+bmjhsmPSuTR8bp4F74tomqP16k67+v0PtvTdH+F15p+cwYReI1318cMVf+k5KtAyyVJrHPgyPaSBK1T9S2bp8baUSj0Fnfr8IhYTRZBdhwSNNFkz3qpH1hfb2+M9GmQoEPm95eWAOIqXNjY8cdkxv1KC2L+l43RGz0K5XqsO5yxfKsASyVJs8yRidNdkZGw8Q8Ji3SJ/3kZd9fnDfVbysAGycL9TumOoFyZbVLzHd7bZW+WjbrADJtlVoCWJoj4tMmAcpoX6e10I4WmjNojAPMkvaFALEnPPnuR8F/bWghEa36fnXQxAC2AND82Z7odQn3fVHC+nC0TC/tasRA2dFCM2uhDYBY+5SjxaIAhN/37yjSJxMzhL8qXpaXd+2hD0bL1rQQ54eVSnVctl+y+YwCNL3vAzSsfXE9LKIWymqurGA75ysM6o50MwrQ8/QYLwCFdQsW5MAzA8G/8Vk4BUJYKtoXClfUQnwGv+nde2t078F6UN7f/9ylza1Nbc5vxtjMwkLlXLJB0Pq0YYCTStMnBFoErOd2d4TUq+Pnf1gIjoxU07HhETo2FB2pCKAB1HtrAejbd+8Ef/F/2WRiGjUGUMX6hBBx9AOtkk0QXKgd13+7EQg3bsLA2f/iq8Gg2d3UbtmyUO9315alBg/2hJVKdUC2bJl8BgHKhUng1ADrWKcUjnocwm483GiZ3mQ6lyTP42l692AwsHrBhSae+/ZLqUGk2z9qDKCs6wz7sfCs7uovP9PNP28pT01JQMV5NpzqcdofGlCXVmp06cZlieL0bieMAfS8icsyQbmfl2etmfIS0lXOErYfZ46wZCWS1mMmgwDlDBi7B60S4lXIIrrvFDRQ62GvSYBcRhbtpryd/ZhMy3rnEeFhDfxs8Www9Ucnvaf1qQNEh9sNmaxDbIcHV5zsdkK3JWoEYDPu5X70aHySwwWIsEYx5YcOBGicCrywt75f1SZ3bQWJsJpXwmRMshbGWYYIePCdhhZzXHjo8LYF2Gk6xdYCG+U0k054rgBMdALfrokKJrp2zrrhOQEQjfS8SSkrtJvEswCxHR4MFfhck7jtnDBidADsNJ3a1MRO8GCwyG0Vek0E7mwjlE4iunUZRgOMh9DRLb9hTjabii4+aJ4eeGiTMwDlXGkyYhYhQgNOf3NG5rHYeUTngl54QZPccKXJOrNlpSzGvZiOYRHPCHXXpftQ18g+sLEGyh0nxQEIDUy+FnWvWTSgdAMkKozofF2JMYC4QlavM9xC0pLC+BXbU6juNVfnJh6CNQawaYlqMWSCheO9U4EhYymOk2IcE0UOVKdCKhoA9QU1ASCSLc9MaIXqHDC61z/jGqgrrFC0Cm2dVIjnlB/P6XrnkGNhhRghpdLk/aRXykxahd3mPbFOxLvIHhd1n0f17v/CeoyugU2Aia+Viedv+rSh95KFWBdoIVLckMXWGvTGwlgDqBJe2E2koQWqGrwLo+fA3iHqf6o/CNJViRsVL8AktUR1+z9FORnXQB3GjGrgE8Ad3v/G/2JNMQAQOQafqkwK6wV4aGGCpNX7kgLA+Bc8xQuZUZrQDVy74GWDcUNLFOsf1sE4aVtc8EyihaIF2mstgvfk7UOjLdHd2PRD43Df4cC+4cevHglBYIuA77u9vUnH2mti62BdA1Fh3JccRFmDAIc80NQwARz2i3i1luhyQx7kDd8hEwVSg/Vr7GKnNSNGHC1TU6WTnHOl1yR10wJYiXBwi+BQF6bZdnDtUx+eKR0Zb7ndhDxY6xC+ER7Yitofzyeq1+/ZaQq3YsSIFctGbIfPiFqATTyEi8/Eq2XIC8ME06HKaTkA9SpL1FZVgJzTuUql2njfl8FkHaDqVNoe+Nsuizjg2svoBlKckuUDd4lsvB8mlSk0rFT1pKLTHT4YIJWmRuoa4N3W0++vLUtvPWB1MlYY1n0Tt1sfrWtg2BDV80JsrHGHL1ir/rylwbXVHTvWV1wyxZU2rIuy03Ia7wxNDSDEpwpRl6aZK8eMu6xXe1MFuL0g2ocH+aUO0HWIzTVvXGeYhMoMkQmAaDDODut1mkt69KTSeQ15VwsFPm76lZKZnkLFxjVfxQyIjdftZjrxWrH477itVytnzgrtxUZ3SKLOcdD46R02q/t9L3HbmJkptL0DuKLGeR2HwRnSxuBXzMq29ngyUDMLsG2/iKAUY+9bixZU/vNz0TKKyIE9I+cMv9xpUSOzCy4UV+Y1sJ1rww1XOME5HzNksa4ifpOxwhdZmiqdMmJk1bXpUx0j4kc5Z0NxgMIoIaJaocCuMFavpbklkO23mM85DezVyUYAFeGXrYc45z1+xo6vEvWtFoubK2lvA+JA27YAkwrDxee3lQa6CCBpm3OASSWY8vM5wJQBJK0+B5hUgik/nwNMGUDS6nOASSWY8vM5wJQBJK0+B5hUgik/nwNMGUDS6nOASSWY8vP/AQEZO620uQnfAAAAAElFTkSuQmCC'";
  bulb = "";
  return bulb;
}

String ServeHand(){
  String hand = "'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHAAAABhCAYAAAAOaiV1AAAJB0lEQVR4Xu2dz28UNxTHnxNKdosovwuCChJQoSClTaSgVqKHhBOnkhyS3VAkQm89lfwFNH9BufVWgtSS2eQA7aknEqQitQKJLZFKCRIJqKBSfhdRyJasq+/sejsZZndnx/bMmMxc+LG2x/M+fs/vPdszjJLLaAkwo3ufdJ4SgIYPggRgAtBwCRje/UQDE4CGS8Dw7icamAA0XAKGdz/RwASg4RIwvPuJBiYADZeA4d1PNDABGJ0EhoZ6V794keogKnZzTh8wxldzzjoYo9XVe8WnSr+x84xRnvOmvGVZc9E9hdydjdPAbDbbyhjvLRb5EcaoQ+7xK7XnOKezTU10fmxs/KyiNkNpxgiAJU17o5dz9oVCaNUEDG08RdQ0aoJmxhpgCdzyY5wTwNUwi7oGOxslYiNxBhlbgIODmWPFIj8eDTj3gGCjqdT88Ojo2ce6hkrQdmMHMJvNdnNe/CoEU9mQzDinx01NbGRsLHeioYqaC8cKYCYzAHDHND+zZPN8KpX6ty8u2hgLgJjrnj9fPhk3ratGGtrIWFOfZVnlkERyTEhUjxzgoUP9HQsLDPAicFIkJGdX5Ucta2JUthWZ+pECNBteRewjljX+pQwEmbqRAXxN4JVlz0YtK3dUBkTQupEAVA1vxfp/aNnyl7YMnj1YQS/nm4PKQ6JeNOY0dIBIhXFevKxizgO4PQdmqGXlfEXwgHfr0jt058omCRhBq4YPMVSAKr3NZS0L1PXpZcKfgAbNgxYCKq6rP+6kB7NrgpIIVA/eaXMz7zl9eiIfqIEAlUIFmM1mThLxoQD9fKXK5vf/pO37btL80xa6PNFeMZs799+gt3fdoyd33qLp73eruFWjbcylUoXOsOLE0AAODg70ck5nGpVGtfJb996mrV1/2Oby1sUtlWKplfPUdThvg734rarFisZ6zTmdyOXGhxurFax0KADLpnNWxbwnHrMaQPz+8ee/2MV++vrDYFJRUqupJ4xAPxSAOlJk8QdIc5Y13qZkLNRoRDtAeJ1ExVnVD2IAQDyy9iA/BIDqHBfnIDABILzSdLrQptOh0QpQl/YBpAkA0U/G2LDOJSjNAPVon0kAifTOhdoA6vA8GzWhNy5sI4QVCO4RVtz9fb0dH4Z9MUZ9ujZLaQOYzfYPEbGTuoTlx4R63fv6ue1099oGXd3ybBc73nK58T4dN9UGMJMZOMMY9erotF8TihTb7IVt9OJpC218776docEFzVzX9ohWrHtGC4VltlYiGYByuq5UqrBGhzOjBWB5N9kjXcLwCxCpNKfJbD94lVZt/tuzW4A9/cMeenb/TS3d1mVGtQBUnTbzkqgfE+rOxIg6aO+vaxtsTUQCfPeBGXueBDxoIbQTF6A+nFtrl5NdotKVXtMCMJsdwAr1cS1DudyoDECAQgJcXCJ/Wq2/KA/tlIHIOeVzufFO1TLRBLB/koh1q+5so15oNQ10J8DRrsifAhaWoqCJMLfQTixZQQtl1xgta1y5vJU3CGFkMgOPVCauVZvQWgDd64hi2Qpri/hN7lKf4FYOMAwHxq8TE0QDf/6ma5GphBbC+VGzvqh+xV45QOysJipOyo3U+rVl5sBaGuiGrhag+uR2ArA8Vjr7p21PdMkDVBVCQJjCncf8447PVGugiBH1AuRTljXRU9+++C+hQQPlQwjsdYHz4LwQt82c2175r1oA4Tki9wnP0XkhXEC921c2eQ4IZGbcjopaE7oEALqDbcReSIHBlXfGb7UA+h+/9UsmAOvLqFICkD767JL9b2fSGea0/ZPfbIhCExOAJbEpN6Eyc6AY7e5MCToqfsPf4UXaoYTHrrQGxouvoktQA4OHEdA0eIMwm5e+63wldbVx1z16d/8NW/CAjPJeIYEvMj4LqQW4BMKIvYfz9lZ5t9Mi5C027op/JwB9jkS/xWQzMQgd4EW650Hn/UXMJsypc2Ov3376LadWAw3IxEAwsrlQpyfqXtND+3BmABGa6vW7Xzh+yqkFaEAuFELJZuVXI4SpxHyYn2h/ZbUcENe1PtS+PUIlQGNWI1SsBwIQQgex0Cq7HudH27zKqAJo1HqgTCjhFKLzCJma5ZzGMQqA1Zwqvy0atSJfMqMD3O/D1SonQoswHJZaGijr7Rq1J6bsyCjbleaM/8LeFqgq46Nj/oOclWdixChWvS9UJLh17x5za6EagPpegqANoI6d2YgPESdWy9SoMNnuNsQyk8yRbV3mU6sGluZBtWcjwvZMncl191aLBgaL1nOC2jSwBFD92UCs6XX0Ty9amWhAmA0VFRuaZPbDGH06SYcWos0wPFNntieo42T8+UBdWoh2dXumwmmSeVmCbu3TPgcKe6XjjDzadm69CKolXjbVOTgkcq1a5z7Rb61zoLiJDo9UtO1cXsLOafc+mIYmPZdmy7WnPnHt9SyhAMSNVaXXvB7CqYlY6J2Z3BHolJF7P45zE1UjA0FX2ixSgOXsjLY38sLste27aXunuJC79HvmD3UBT7xzTSbviaR1Ol3o0XEWMHKAKt+V5vUwCDGw5cJ5BhAa+WBuLT25vXJRFZRdteUprW19WIGOBMH1yR2B37H22r8rTXilqt5WWM2sASBiOLExuJ75s8/PX9tgnz6SOUIWxRt8Q5sDnUJU/b7QaoBgTgETcaP7ZK54w6HXru96wL1/V79dwk8/IgGIjoUF0Y8Q5MtEAy+0OLCagF4PiNHBixyg0MRikeE1lK3ymhB2C9HCiwVAdKL8caszuo9lq8IbhbdZre+RzYFeHVKxGUoVpGrt4KU96XThaFhxXr3niRVAh3NzMm5fcSl9rYUPR/2hDzfQ2AEUHcSWDM4ZMjeRf9EFqbF0ujASF61zQowtwP/nxuT7gbXMaKwBOjte2iRFR0JwdOwveKZShRNx1DhjTGi1USe+oct58aAqmEhAE9FUczM/FeY3H+o5KH5+N0YDawDtZox34CvWRMVWIoZ40jOmLDsiec7ZY8boV3zFuqWlMGWCphkRRvgZcUmZxRIwXgOXOtAEoOEjIAGYADRcAoZ3P9HABKDhEjC8+4kGJgANl4Dh3U80MAFouAQM736igYYD/A9NdXitMhT26QAAAABJRU5ErkJggg=='";
  return hand;
}

String ServeLightning(){
  String lightning = "'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHAAAABhCAYAAAAOaiV1AAAIi0lEQVR4Xu2dT2gUVxjAvzfbulkLVm0KBYvV/kMPFSOFHFpocil4aiy4O7GHJrS3ojEHydHkGDxold4qiQfN7Ao1PQm9uEIvQqnb9qC0paaiUNCqCE1MSvaVb2dn87Ld2ZnZfd/7Q+aBB7Mzb958v/ne+77vfe89BmmxWgLM6tanjYcUoOUfQQowBWi5BCxvfqqBKUDLJWB581MNTAFaLgHLm59qYArQcglY3vxUA1OAlkvA8uanGpgCNE8CruvuAgD8VyuM8f0AfIFz50nwt56eZ5XZ2fnG/817i3gtsl4Djxw5vJ9zZ6Ba5R8gNMZgf7xXB+AcnjDGK5yziuPA9Wx2pWwbVCsBuq47AMA/BeADoqbFBdfuOs6h4jjsQja7PGsDTGsAjowMbV1ezo5wzsdkQwsHymYB2AXP88oyPg6KOowHiOCePdt0nHMYYwy2Ugghuk5eBshMmQjSaICue3iEc3ZaH7j1aDmHecaccc/zFqKhq7nCSIC+Fbk6A8BwjDOqoOHjOGxqbq54xoSGGQfQNK0Lh8TLPT3/HtJt6BgF0HULMwB8xIQvO04bUBszGT546dLlSpzrKa4xAiAaKktLm64l8eEohNF5nXzU8y7Pdn5/53dqBygbXu/2Zeh9aTmxRO7eewEWlzKJ71u7QQ9ErQBlw9vz1lOYOHqrIwhfTLzbJUB8rHqI2gDKhofiG/74T/hw4K/EAO/e3wwnp99JfF/rG9RC1AawUMhfYQyGJEmtVs2pyQpgF4oFoSwuPRda/c4d/8Dm3Grt92+vvgrzV3dIa4rj8D5Vho0WgK6bnwSAk9IkBlADhwCDEtUlfjX9QwMgah8Cl1XQOs3lVnarcDGUA/QD0dVrsoQV1INdJ3ahWG7/vgWmz+4NfYQ4VqLhgrDlF172vMuD8utdX6NSgPW45k2KYPTRz3+FA/se195u7pvX4LvyK6GyE8fK72+8DOcvvk4iZ8bYOHXERinAQiGPcc3jFNKaOXujUW1Ulzg18Qvs3LFYux7hIUSK4s83On2UsVNlAP34ZvUOhaBQ81ADsTx8lIUTk+FzuknHym7biwHwYrF0qNt6wu5XCPDwNargdJIu8f3+B/DZJ3/U5CHXfWiHyBmkmopSApDKcAlEJroP575+G378eVuoNMWxUrb7EI6QzqBRBJAuSJ20S6R0H9rqIJFvSA6QcuxDgZnnPoSOVrOeVxyVPRYqACjfaReFYKL7EAapp2dlm2znXgVAtDwbOZoyv0AMhWGXGBRT3Ic2Y6H0aSdSgJizWa0ydNxJisnuQ6sXpnApSAFSxDxFwaA7gG4BlqiIijhWqnMf/o/R80pSZS61submui6d74fP6tR9QGcf/7Uri4sZOH/xDQlzhM1PkesTEgPMc5K+E6AWCsOQWFBGj/W3fZQYaovbJvQn0a+UXKY8r4SzMVIKGUDq8U/sEqMELY6VSaQWNauRpK61a+U69WQAMT0QgM109pLRd00cuwV73nxauzBq9iFungxatUFMFevFGQ2sW3JZ8LzSbll1EgKk8/+a3QcMXkeNaXEEJhpFOE94YrKPYAwEkGnIEAKkC58lcR/igMNrmhOiKKeZZKZcEAKks0BFTZHVzYlzhDRjn/gpybNErQSYxH2Io4GiQYTXR0V04tTZ/poNDDCp+xAlbBxPT03ebCQ4ydLoFGCIBJK4D1Hw8HdVhsv6tmxgDRTHqij3IQqgSsMlBQhQ6+bE2Ydu3Qe1hotlRgxF5nWzxmBKRJzSKutaveFiGUCKmYhOFq+0mnnQY7isAbTCkR8eLhznnJ+OoyFJrhG7vTj3tZpm0mO4WAaQKhMNtefAvkfQu30llN97/Q8ai1yaDR19hkvQXEuC2fU0ej/XXXERtXT63F64/duWRgv0GS5+EziHM8ViaVyWSMgiMdjAQiF/U8eyaXHuT1ylpNdwaWig1LwYaoBkayHCvmCxixTT7HUbLmvtdXbLXCtBCnB4OD/EOVyR1V3EqUdMnReD0roNl3r3WSkWS31x3iPuNaQA693oY5U7LYnrJILUef2GC834h7WSA1S994s4Ux+sk9BtuFB1n4oA0qzIDetimtc+oPYFK3fxHvqpotYtw20sZXefSgDiQ1w3T5adLYqr1UIX9VNFYZ8Wze4V5F2oD5A2wSkQmTjWoQHz8O9sI/GXMsclhsEhNZFJfJ4SgKq0cOjgffjo4L3a+6ELEWw5gv+nzHGJBkijfcq6UFVaKK5UEoVKn+PSFiGZ9ikFWIdItswa6w8LdOsyXHys8mbfW30myrpQfDh1tnar9Hk1OS6hlifpBgfKNbA+FkrfpQnrbTVXqNNwUbVbk1INDL5TilVLYggteI5Ow4UxODQ3V5qPNnC6u0ILwPpOhXdkhtiadyrUbLhIXYHUDrEWgMF4uLrKcJdeKUcJiCE0rF+f4cJINjMIg6gNoD8eyguziQaMLsOFKlxmpAYK46GUZWhBDFSX4YLwcrmVQdm7UESNkFo1cA2iO8B5FTeA7ao7RUMGIzBiCkWUAGT8jpsX5HIro6rhaXEjwgRW9xFx8pdkSxIZoFrXoXbMa26DERoYNMpPhHr+CtWmeDIh1o+uG9d13EDwLkYBXOtS6Vb3yoCI410mw0dV7YtttBET1jgTz0/ytQ6+lLnLRLcflJEaKL5UPTEKM7w1j421swTxCDpjTi4zyoiJ+hLrh2LhGYKxj1iNqjPqd1/j2LyJ4IweA9sJ1rdWnTHO+VC3bkfYc2w6htX4LrQdTOxeq1XAw48HutFM1DQAKDsOu845mzetm7TSiInq3lr97ofmVncBsF2cw4v+8ePrC+cMDZGf/L86eDbugk3AjPYDO4G20e+xugvd6PCsskJTWK0lkGqg5V9GCjAFaLkELG9+qoEpQMslYHnzUw1MAVouAcubn2qg5QD/A1TZaJ6x9WuiAAAAAElFTkSuQmCC'";
  lightning = "";
  return lightning;
}
