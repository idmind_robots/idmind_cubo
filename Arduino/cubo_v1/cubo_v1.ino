/********************************************
   Author: Carlos Neves
   Project: IDMind's Cubo
   Goals:
   1. Set up a WiFi Access Point
   2. Set up a HTTP Server
   3. Control the lights using the webpage
*/
#include <Wire.h>
#include <Adafruit_NeoPixel.h>


// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN_NeoPixel            6
#define PIN_PushButton          5
#define PIN_Vibrator            2

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      8

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN_NeoPixel, NEO_GRB + NEO_KHZ800);
int delayval = 500; // delay for half a second
int buttonState;             // the current reading from the input pin
int neoPixel_State = 0;
int Neo_RED;
int Neo_GREEN;
int Neo_BLUE;
int reading = 0;
unsigned long timeout = 10000;
String main_buffer = "";
String esp_buffer = "";

String ReadUSB() {
  main_buffer = "";
  while (Serial.available() > 0) {
    char c = Serial.read();
    main_buffer += c;
    delay(1);
  }
  return main_buffer;
}

void setup()
{
  delay(5000);
  // Start NEOPixels
  pixels.begin();               // Initialize NeoPixel Library
  pinMode(PIN_PushButton, INPUT);
  pinMode(PIN_Vibrator, OUTPUT);
  neopixel_start();

  // Communication to PC
  Serial.begin(115200);

  // Communication to ESP8266
  Serial1.begin(115200);
  Serial1.setTimeout(10000);


  while (!Attention()) {
    Serial.println("Communication to ESP8266 failed");
    delay(1000);
  }
  Serial.println("Communication to ESP8266 successfull");

  while (!AccessPoint()) {
    Serial.println("Failed to set ESP8266 as WiFi Server");
    delay(1000);
    neopixel_start();
  }

  while (Serial1.available()) Serial1.read();
  Serial.println("ESP Buffer cleared");
  delay(1000);
}

void loop() {
  //  // put your main code here, to run repeatedly:
  //  for(int i=0; i<NUMPIXELS;i++){
  //    pixels.setPixelColor(i, pixels.Color(255,0,0));
  //    pixels.show();
  //    delay(10);
  //    //Serial.println("Turning LED ON");
  //  }
  //  for(int i=0; i<NUMPIXELS;i++){
  //    pixels.setPixelColor(i, pixels.Color(0,0,0));
  //    pixels.show();
  //    delay(10);
  //    //Serial.println("Turning LED OFF");
  //  }

  //  String aux = ReadUSB();
  //  if(aux.length() > 0) SendCommand(aux, true);
  //  delay(10);

  HttpServer();
}
