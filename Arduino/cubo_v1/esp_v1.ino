/************************************************
 * Returns the response from the ESP8266 Module
************************************************/
String GetResponse(){
  String responseBuffer;
  while(Serial1.available() > 0){
    char c = Serial1.read();
    responseBuffer += c;   
    delay(5); 
  }
  return responseBuffer;
}

/***********************************************************
 * Sends a command to the ESP Module and waits for response
 ***********************************************************/
bool SendCommand(String AT_command, bool debug){
  debug = false;
  Serial1.println(AT_command);
  delay(10);
  String resp = GetResponse();
  if(debug) Serial.print(resp);
  if(resp.length() == 0 or resp.endsWith("ERROR\r\n")){    
    return false;
  }
  else if(resp.endsWith("OK\r\n")) return true;
  else return true;
}

/***********************************************************
 * Calls for Atttention
 ***********************************************************/
bool Attention(){
  if(SendCommand("AT", true)) return true;
  else return false;
}

/***********************************************************
 * Checks Firmware
 ***********************************************************/
bool Firmware(){
  // Check firmware
  if(SendCommand("AT+GMR", true)) return true;
  else return false;
}

/***********************************************************
 * Sets up ESP8266 as an AcessPoint:
 * 1. Sets AcessPoint mode
 * 2. Configures WiFi network
 * 3. Sets MultiChannel Access
 * 4. Opens SocketServer at port 80
 ***********************************************************/
bool AccessPoint(){
  // Set as Access Point
  // Start by setting working mode (1-client; 2-AP; 3-client+AP)
  
  if(SendCommand("AT+CWMODE=2", true)){
    neopixel_green_light(0);
    delay(1000);
  }
  else{
    neopixel_red_light(0);
    return false;
  }
  
  // Configure the WIFi Network
  // AT+CWSAP=<ssid>,<pass>,<channel>,<security>
  // Security can be 0 - None, 1 - WEP, 2 - WPA_PSK, 3 - WPA2_PSK, 4 - WPA_WPA2_PSK
  if(SendCommand("AT+CWSAP=\"Cubo\",\"asdf\",11,0", true)){
    neopixel_green_light(1);
    delay(1000);
  }
  else{
    neopixel_red_light(1);
    return false;
  }

  // Set Multi Channel Connection
  if(SendCommand("AT+CIPMUX=1", true)){
    neopixel_green_light(2);
    delay(1000);
  }
  else{
    neopixel_red_light(2);
    return false;
  }

  // Set CIPSERVER
  if(SendCommand("AT+CIPSERVER=1,80", true)){
    neopixel_green_light(3);
    delay(1000);
  }
  else{
    neopixel_red_light(3);
    return false;
  }

  return true;
}

/***********************************************************
 * List Clients connected to ESP8266
 * - TODO: This should return an array with the IP's
 ***********************************************************/
int ListClients(){
  int total_clients = 0;
  Serial1.println("AT_CWLIF");
  delay(10);
  String resp = GetResponse();
  Serial.println("resp");
  Serial.println("WARN: NOT PARSED");  
  return total_clients;
}

//String ReadLine(bool debug){
//  String esp_buffer = "";
//  while(true){
//    if(Serial1.available()){
//      char c = Serial1.read();
//      if(debug) Serial.print(c);
//      esp_buffer += c;
//      if(esp_buffer.endsWith("\r\n"))
//        return esp_buffer;
//    }
//  }
//}

String ReadCharacters(int total, bool debug){
  String esp_chars = "";
  int counter = 0;
  while(counter < total){
    if(Serial1.available()){
      char c = Serial1.read();
      if(debug) Serial.print(c);
      esp_chars += c;
      counter++; 
    }
  }
  return esp_chars;  
}
/***********************************************************
 * Server Responses 
 * - TODO: Everything
 * Format: +IPD,<channel>,<length>,<format>
 * +IPD,0,422:GET / HTTP/1.1
 * Received Request
 * Host: 192.168.4.1
 * Connection: keep-alive
 * DNT: 1
 * Upgrade-Insecure-Requests: 1
 * User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.75 Safari/537.36
 * Accept: text/html,application/xhtml+xml,...
 * Accept-Encoding: gzip, deflate
 * Accept-Language: en-GB,en-US;q=0.9,en;q=0.8
 ***********************************************************/
void HttpServer(){
  if(Serial1.available()){
    char c = Serial1.read();
    esp_buffer += c;  
    if(esp_buffer.endsWith("\r\n")){ 
      // Handle Request!
      if(esp_buffer.endsWith("CONNECT\r\n")){
        Serial.print("Started connection ");
        Serial.println(esp_buffer[0]);
      }
      else if(esp_buffer.endsWith("CLOSED\r\n")){
        Serial.print("Closed connection ");
        Serial.println(esp_buffer[0]);
      }
      else if(esp_buffer.startsWith("+IPD")){
        char data[100];
        int connection_id = esp_buffer[5]-'0';
        int sep = esp_buffer.indexOf(":");
        int req_length = esp_buffer.substring(7, sep).toInt();
        esp_buffer.substring(sep+1).length();
        sprintf(data, "Request of %d, with length %d", connection_id, req_length);
        Serial.println(data);
        Serial.print(esp_buffer.substring(sep+1));
        String HttpRequest = ReadCharacters(req_length - esp_buffer.substring(sep+1).length(), false);        
        ServeHomepage(connection_id);
      }
      Serial.print(esp_buffer);
      esp_buffer = "";     
    }
  }
}
