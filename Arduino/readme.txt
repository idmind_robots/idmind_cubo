Using ArduinoIDE:
1. Install ArduinoIDE
2. Install TeensyDuino - https://www.pjrc.com/teensy/teensyduino.html
3. Import libraries from .zip: Adafruit_MMA8451, Adafruit_Sensor
4. Unpack PN532 files and copy to Arduino/libraries folder
