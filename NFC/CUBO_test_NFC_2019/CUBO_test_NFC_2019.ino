/**************************************************************************/
/*!
    @file     Adafruit_MMA8451.h
    @author   K. Townsend (Adafruit Industries)
    @license  BSD (see license.txt)

    This is an example for the Adafruit MMA8451 Accel breakout board
    ----> https://www.adafruit.com/products/2019

    Adafruit invests time and resources providing this open source code,
    please support Adafruit and open-source hardware by purchasing
    products from Adafruit!

    @section  HISTORY

    v1.0  - First release
*/
/**************************************************************************/

#include <Wire.h>
#include <Adafruit_MMA8451.h>
#include <Adafruit_Sensor.h>
#include <PN532_HSU.h>
#include <PN532.h>
#include <NfcAdapter.h>
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

// Which pin on the Arduino is connected to the NeoPixels?
// On a Trinket or Gemma we suggest changing this to 1
#define PIN_NeoPixel            6
#define PIN_PushButton          5
#define PIN_Vibrator            2

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS      8

// When we setup the NeoPixel library, we tell it how many pixels, and which pin to use to send signals.
// Note that for older NeoPixel strips you might need to change the third parameter--see the strandtest
// example for more information on possible values.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN_NeoPixel, NEO_GRB + NEO_KHZ800);

int delayval = 500; // delay for half a second
int buttonState;             // the current reading from the input pin
int neoPixel_State=0;
int Neo_RED;
int Neo_GREEN;
int Neo_BLUE;
int reading = 0;

PN532_HSU pn532hsu(Serial2);
NfcAdapter nfc = NfcAdapter(pn532hsu);
byte uid[4] = { 0x00, 0xFF, 0xAA, 0x17 };
byte uidFromTag[sizeof(uid)]; 
int Count_Led;


Adafruit_MMA8451 mma = Adafruit_MMA8451();

void setup(void) {


    // This is for Trinket 5V 16MHz, you can remove these three lines if you are not using a Trinket
#if defined (__AVR_ATtiny85__)
  if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
#endif
  // End of trinket special code

pixels.begin(); // This initializes the NeoPixel library.

  
  Serial.begin(9600);
  
  Serial.println("Adafruit MMA8451 test!");
  

  if (! mma.begin()) {
    Serial.println("Couldnt start");
    while (1);
  }
  Serial.println("MMA8451 found!");
  
  mma.setRange(MMA8451_RANGE_2_G);
  
  Serial.print("Range = "); Serial.print(2 << mma.getRange());  
  Serial.println("G");

  pinMode(PIN_PushButton, INPUT);
  pinMode(PIN_Vibrator, OUTPUT);

  Neo_RED=10;Neo_GREEN=10;Neo_BLUE=0;
  nfc.begin();
  
}

void loop() {

  if (nfc.tagPresent(50))
  {
        NfcTag tag = nfc.read();       
        tag.getUid(uidFromTag, sizeof(uidFromTag));
        Serial.println(uidFromTag[0]);Serial.println(uidFromTag[1]);Serial.println(uidFromTag[2]);Serial.println(uidFromTag[3]);

        if (uidFromTag[0]==169 )
        {
          for (Count_Led=0;Count_Led<8;Count_Led++)
          {
            pixels.setPixelColor(Count_Led, pixels.Color(200,200,0)); 
          }
        }
        else if (uidFromTag[0]==153 )
        {
          for (Count_Led=0;Count_Led<8;Count_Led++)
          {
            pixels.setPixelColor(Count_Led, pixels.Color(0,200,200)); 
          }
        }
           pixels.show();
  }
  else
  {
  // Read the 'raw' data in 14-bit counts
  mma.read();
  Serial.print("X:\t"); Serial.print(mma.x); 
  Serial.print("\tY:\t"); Serial.print(mma.y); 
  Serial.print("\tZ:\t"); Serial.print(mma.z); 

  reading = digitalRead(PIN_PushButton);
  if (reading == HIGH)
  {
     neoPixel_State++;
     if (neoPixel_State>5) neoPixel_State=0;

     if (neoPixel_State==0) {Neo_RED=0;Neo_GREEN=150;Neo_BLUE=0;}
     else if (neoPixel_State==1) {Neo_RED=150;Neo_GREEN=150;Neo_BLUE=0;}
     else if (neoPixel_State==2) {Neo_RED=150;Neo_GREEN=0;Neo_BLUE=0;}
     else if (neoPixel_State==3) {Neo_RED=150;Neo_GREEN=0;Neo_BLUE=150;}
     else if (neoPixel_State==4) {Neo_RED=0;Neo_GREEN=0;Neo_BLUE=150;}
     else if (neoPixel_State==5) {Neo_RED=0;Neo_GREEN=150;Neo_BLUE=150;}    
      Serial.print("HIGH --"); Serial.print(neoPixel_State);
  }
  else
  {
  Serial.print("LOW"); 
    
  }

 for(int i=0;i<NUMPIXELS;i++){

    // pixels.Color takes RGB values, from 0,0,0 up to 255,255,255
    pixels.setPixelColor(i, pixels.Color(0,0,0)); // Moderately bright green color.
    }
//pixels.setPixelColor(2, pixels.Color(200,0,0)); // Moderately bright green color.
//pixels.setPixelColor(6, pixels.Color(200,0,0)); // Moderately bright green color.
//pixels.show();
  
  if (mma.z>2000)
  {
      pixels.setPixelColor(4, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.setPixelColor(5, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.setPixelColor(6, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.setPixelColor(7, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.show(); // This sends the updated pixel color to the hardware.    
      Serial.print("---0---"); 
    //  digitalWrite(PIN_Vibrator, LOW);  
  }
  else if (mma.z<-2000)
  {
      pixels.setPixelColor(0, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.setPixelColor(1, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.setPixelColor(2, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.setPixelColor(3, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.show(); // This sends the updated pixel color to the hardware.       
      Serial.print("---1---"); 
      digitalWrite(PIN_Vibrator, LOW);  
  }
  else if (mma.x>2000)
  {
      pixels.setPixelColor(1, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.setPixelColor(2, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.setPixelColor(6, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.setPixelColor(7, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.show(); // This sends the updated pixel color to the hardware.      
      Serial.print("---2---"); 
      digitalWrite(PIN_Vibrator, HIGH);  
      if (mma.x>5000)  digitalWrite(PIN_Vibrator, HIGH);  
      else digitalWrite(PIN_Vibrator, LOW);  
  }
  else if (mma.x<-2000)
  {
      pixels.setPixelColor(0, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.setPixelColor(3, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.setPixelColor(4, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.setPixelColor(5, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.show(); // This sends the updated pixel color to the hardware.      
      Serial.print("---3---"); 
      if (mma.x<-5000)  digitalWrite(PIN_Vibrator, HIGH);  
      else digitalWrite(PIN_Vibrator, LOW);  
      
    
  }
  else if (mma.y>2000)
  {
      pixels.setPixelColor(0, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.setPixelColor(1, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.setPixelColor(4, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.setPixelColor(7, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.show(); // This sends the updated pixel color to the hardware.      
      Serial.print("---4---"); 
      if (mma.y>5000)  digitalWrite(PIN_Vibrator, HIGH);  
      else digitalWrite(PIN_Vibrator, LOW);  
    //  digitalWrite(PIN_Vibrator, HIGH);  
    
  }
  else if (mma.y<-2000)
  {
     pixels.setPixelColor(2, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.setPixelColor(3, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.setPixelColor(5, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.setPixelColor(6, pixels.Color(Neo_RED,Neo_GREEN,Neo_BLUE)); // Moderately bright green color.
      pixels.show(); // This sends the updated pixel color to the hardware.      
      Serial.print("---5---"); 
     if (mma.y<-5000)  digitalWrite(PIN_Vibrator, HIGH);  
      else digitalWrite(PIN_Vibrator, LOW);  
   //   digitalWrite(PIN_Vibrator, HIGH); 
    
  }
    Serial.println();
    delay(50);
  }
 
  
}
